package org.pgstreaming;

import org.pgstreaming.dsl.EngineBuilder;
import org.pgstreaming.dsl.Flag;

/**
 * PGStreaming DSL entrypoint, allowing you to build data streaming application for PostgreSQL with ease.
 * 
 * eg:
 * {@code
 *   import static org.pgstreaming.PGStreaming.*;
 *   public class HIDSLTest
 *   {
 *       public static void main(String[] args)
 *       {
 *           // use the PGStreaming DSL to define our streaming application
 *           pgStreaming()
 *           .source()
 *           .name("policies")
 *           .ofRecord()
 *           .from("jdbc:postgresql://127.0.0.1:5414/streaming", "rep", "")
 *           .slot("welcome_email")
 *           .wal2json()
 *           .includeTable("event.policy")
 *           .then()
 *           .consume((key, value) -> {
 *               System.out.println(
 *                   "To: " + value.get("customer_email") + "\n" + 
 *                   "Welcome " + value.get("customer_name") + "\n"
 *                   "Your policy number is " + value.get("policy_reference") +
 *                   " for your property at " + value.get("post_code")
 *               );
 *           })
 *           .register()
 *           .launch();
 *       }
 *   }
 * }
 */
public final class PGStreaming
{
    private PGStreaming() {}
    
    /**
     * The processor should commit it's work automatically upon processing the event
     */
    public static final Flag AUTO_COMMIT = Flag.AUTO_COMMIT;
    
    /**
     * Entrypoint to PGStreaming DSL
     */
    public static EngineBuilder pgStreaming()
    {
        return engine();
    }
    
    /**
     * Build a PGStreaming engine
     */
    public static EngineBuilder engine()
    {
        return new EngineBuilder();
    }
}
