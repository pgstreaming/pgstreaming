package org.pgstreaming.dsl;

import java.util.Objects;
import java.util.function.Consumer;

import org.pgstreaming.PGStreamingConnector;
import org.pgstreaming.decoder.DecoderPlugin;
import org.pgstreaming.decoder.decoderbufs.Decoderbufs;
import org.pgstreaming.decoder.wal2json.Wal2Json;
import org.pgstreaming.util.Require;

public class SourceBuilder<K,V>
{
    private final EngineBuilder engine;
    
    private final String name;
    
    private final Class<K> keyType;
    
    private final Class<V> valueType;
    
    private String url;
    
    private String username;
    
    private String password;
    
    private String slot;
    
    private DecoderPlugin<?> decoder;
    
    public SourceBuilder(EngineBuilder engine, String name, Class<K> keyType, Class<V> valueType)
    {
        super();
        this.engine = engine;
        this.name = Require.nonEmpty(name);
        this.keyType = Objects.requireNonNull(keyType);
        this.valueType = Objects.requireNonNull(valueType);
    }
    
    public String name()
    {
        return this.name;
    }
    
    public Class<K> keyType()
    {
        return this.keyType;
    }

    public Class<V> valueType()
    {
        return this.valueType;
    }

    public SourceBuilder<K,V> configure(Consumer<SourceBuilder<K,V>> configurer)
    {
        configurer.accept(this);
        return this;
    }
    
    /**
     * Set the details of the PostgreSQL server to source events from
     * @param url the JDBC URL eg: {@code jdbc:postgresql://127.0.0.1:5432/streaming}
     * @param username the database user name to connect with, must have the replication role
     * @param password the database password to connect with
     */
    public SourceBuilder<K,V> from(String url, String username, String password)
    {
        this.url = Require.nonEmpty(url);
        this.username = Require.nonEmpty(username);
        this.password = Objects.requireNonNull(password);
        return this;
    }
    
    /**
     * Set the PostgreSQL replication slot to use for this source
     * @param slot the replication slot name
     */
    public SourceBuilder<K,V> slot(String slot)
    {
        this.slot = Require.nonEmpty(slot);
        return this;
    }
    
    /**
     * Set the PostgreSQL Logical Decoding Output plugin to use
     * @param decoder the PostgreSQL Logical Decoding Output plugin
     */
    public SourceBuilder<K,V> decoder(DecoderPlugin<?> decoder)
    {
        this.decoder = Objects.requireNonNull(decoder);
        return this;
    }
    
    /**
     * Use the {@code wal2json} Logical Decoding Output plugin
     */
    public SourceBuilder<K,V> wal2json()
    {
        return decoder(new Wal2Json());
    }
    
    /**
     * Use the {@code decoderbufs} Logical Decoding Output plugin
     */
    public SourceBuilder<K,V> decoderbufs()
    {
        return decoder(new Decoderbufs());
    }
    
    /**
     * Set the table names from which events should be received
     * @param tables the tables to get events from
     */
    public SourceBuilder<K,V> includeTable(String... tables)
    {
        Objects.requireNonNull(this.decoder).withIncludeTable(tables);
        return this;
    }
    
    /**
     * Create a processing pipeline directly attached to this source
     */
    public PipelineSourceBuilder<K,V> then()
    {
        return new PipelineSourceBuilder<>(this.engine, this.name, this);
    }
    
    /**
     * Register this source with the PGStreaming engine
     */
    public SourceBuilder<K,V> register()
    {
        this.engine.registerSource(this);
        return this;
    }
    
    /**
     * Build the connector
     */
    PGStreamingConnector<K, V> buildConnector()
    {
        return new PGStreamingConnector<K, V>(this.url, this.username, this.password, this.slot, this.decoder, this.decoder.createParser(this.keyType, this.valueType));
    }
}
