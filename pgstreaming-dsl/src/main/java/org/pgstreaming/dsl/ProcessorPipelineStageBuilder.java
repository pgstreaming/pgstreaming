package org.pgstreaming.dsl;

import java.util.List;
import java.util.Objects;

import org.pgstreaming.processor.Processor;

public class ProcessorPipelineStageBuilder<IK, IV, OK, OV> extends PipelineStageBuilder<IK, IV, OK, OV>
{    
    private final Processor<IK, IV, OK, OV> processor;

    public ProcessorPipelineStageBuilder(PipelineBuilder<IK, IV> parent, String stageName, Processor<IK, IV, OK, OV> processor)
    {
        super(parent, stageName);
        this.processor = Objects.requireNonNull(processor);
    }
    
    @Override
    public String toStageString()
    {
        return this.stageName() + " apply(" + this.processor.toString() + ")";
    }

    public Processor<IK, IV, OK, OV> processor()
    {
        return this.processor;
    }
    
    @Override
    public List<Processor<?,?,?,?>> processors()
    {
        List<Processor<?,?,?,?>> processors = this.parent().processors();
        processors.add(this.processor());
        return processors;
    }
}
