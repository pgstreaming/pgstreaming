package org.pgstreaming.dsl;

import org.pgstreaming.model.record.Record;
import org.pgstreaming.util.Require;

public class BaseSourceBuilder
{
    private final EngineBuilder engine;
    
    private String name = "default";
    
    BaseSourceBuilder(EngineBuilder engine)
    {
        super();
        this.engine = engine;
    }

    /**
     * Set the name for this source
     * @param name the source name
     */
    public BaseSourceBuilder name(String name)
    {
        this.name = Require.nonEmpty(name);
        return this;
    }
    
    /**
     * Set the data types of this source
     * @param <K> the key type
     * @param <V> the value type
     * @param keyType the key type
     * @param valueType the value type
     */
    public <K, V> SourceBuilder<K,V> of(Class<K> keyType, Class<V> valueType)
    {
        return new SourceBuilder<>(this.engine, this.name, keyType, valueType);
    }
    
    /**
     * This source uses generic {@link Record} types for the key and value
     */
    public SourceBuilder<Record, Record> ofRecord()
    {
        return of(Record.class, Record.class);
    }
    
    /**
     * This source uses generic {@link Record} types for the value and {@link Void} keys
     */
    public SourceBuilder<Void, Record> ofKeylessRecord()
    {
        return of(Void.class, Record.class);
    }
}
