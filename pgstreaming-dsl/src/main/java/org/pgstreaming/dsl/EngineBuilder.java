package org.pgstreaming.dsl;

import java.util.HashMap;
import java.util.Map;

import org.pgstreaming.topology.PGStreamingEngine;
import org.pgstreaming.util.Require;

public class EngineBuilder
{
    private String name = "default";
    
    private Map<String, SourceBuilder<?,?>> sources = new HashMap<>();
    
    private Map<String, PipelineBuilder<?,?>> pipelines = new HashMap<>();
    
    public EngineBuilder()
    {
        super();
    }
    
    /**
     * Set the name of this PGStreaming engine
     * @param name the engine name
     */
    public EngineBuilder name(String name)
    {
        this.name = Require.nonEmpty(name);
        return this;
    }
    
    // sources
    
    /**
     * Define a source in this PGStreaming engine, which can be used by a Pipeline
     */
    public BaseSourceBuilder source()
    {
        return new BaseSourceBuilder(this);
    }
    
    /**
     * Define a source in this PGStreaming engine, which can be used by a Pipeline
     * @param name the source name
     */
    public BaseSourceBuilder source(final String name)
    {
        return this.source().name(name);
    }
    
    // pipelines
    
    /**
     * Define a pipeline in this PGStreaming engine, which will consume a Source and process it
     */
    public BasePipelineBuilder pipeline()
    {
        return new BasePipelineBuilder(this);
    }
    
    /**
     * Define a pipeline in this PGStreaming engine, which will consume a Source and process it
     * @param name the pipeline name
     */
    public BasePipelineBuilder pipeline(final String name)
    {
        return this.pipeline().name(name);
    }
    
    // internals
    
    void registerSource(SourceBuilder<?,?> source)
    {
        this.sources.put(source.name(), source);
    }
    
    void registerPipeline(PipelineBuilder<?,?> pipeline)
    {
        this.pipelines.put(pipeline.name(), pipeline);
    }
    
    // build
    
    /**
     * Create a PGStreaming Engine configured with the defined pipelines
     * @return the configured {@link PGStreamingEngine} engine
     */
    public PGStreamingEngine createEngine()
    {
        PGStreamingEngine engine = new PGStreamingEngine(this.name);
        // register the pipelines
        for (PipelineBuilder<?,?> pipeline : this.pipelines.values())
        {
            engine.registerPipeline(pipeline.buildExecutor());
        }
        return engine;
    }
    
    /**
     * Create and launch a PGStreaming Engine configured with the defined pipelines
     */
    public void launch()
    {
        PGStreamingEngine engine = this.createEngine();
        // Setup a shutdown hook to stop the pipelines
        Runtime.getRuntime().addShutdownHook(new Thread(engine::shutdown));
        // init
        engine.init();
        // start
        engine.start();
    }
}
