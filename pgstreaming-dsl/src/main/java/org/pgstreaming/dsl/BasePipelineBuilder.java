package org.pgstreaming.dsl;

import org.pgstreaming.util.Require;

public class BasePipelineBuilder
{
    private final EngineBuilder engine;
    
    private String name = "default";
    
    BasePipelineBuilder(EngineBuilder engine)
    {
        this.engine = engine;
    }
    
    /**
     * Set the name of this pipeline
     * @param name the pipeline name
     */
    public BasePipelineBuilder name(String name)
    {
        this.name = Require.nonEmpty(name);
        return this;
    }
    
    /**
     * Set the source this pipeline
     * @param <K> the key type
     * @param <V> the value type
     * @param source the pipeline source
     */
    public <K, V> PipelineBuilder<K, V> source(SourceBuilder<K,V> source)
    {
        return new PipelineSourceBuilder<>(this.engine, this.name, source);
    }
}
