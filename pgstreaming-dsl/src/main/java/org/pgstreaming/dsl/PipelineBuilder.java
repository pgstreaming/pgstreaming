package org.pgstreaming.dsl;

import static org.pgstreaming.util.FlagUtil.*;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;

import org.pgstreaming.PGStreamingConnector;
import org.pgstreaming.processor.Processor;
import org.pgstreaming.processor.adapter.ProcessorConsumerAdapter;
import org.pgstreaming.processor.adapter.ProcessorFunctionAdapter;
import org.pgstreaming.topology.executor.pipeline.PipelineExecutor;

public abstract class PipelineBuilder<K, V>
{   
    // pipeline properties
    
    abstract EngineBuilder engine();
    
    public abstract String name();
    
    public abstract String stageName();
    
    public abstract int stage();
    
    public abstract SourceBuilder<?,?> source();
    
    public abstract String toStageString();
    
    public abstract List<PipelineBuilder<?,?>> stages();
    
    public abstract List<Processor<?,?,?,?>> processors();
    
    // pipeline stages
    
    /**
     * Apply a processor to this stream, which will take and event apply some kind of transformation and pass a new event downstream
     * @param <OK> the output key type of the processor
     * @param <OV> the output value type of the processor
     * @param stageName the name of this stage
     * @param processor the processor to apply to the stream
     */
    @SuppressWarnings("unchecked")
    public <OK, OV> PipelineBuilder<OK, OV> apply(String stageName, Processor<K, V, OK, OV> processor)
    {
        return (PipelineBuilder<OK, OV>) new ProcessorPipelineStageBuilder<K, V, OK, OV>(this, stageName, processor);
    }
    
    /**
     * Apply a processor to this stream, which will take and event apply some kind of transformation and pass a new event downstream
     * @param <OK> the output key type of the processor
     * @param <OV> the output value type of the processor
     * @param processor the processor to apply to the stream
     */
    public <OK, OV> PipelineBuilder<OK, OV> apply(Processor<K, V, OK, OV> processor)
    {
        return this.apply(null, processor);
    }
    
    /**
     * Apply a {@link BiFunction} to this stream, which will take and event apply some kind of transformation and pass a new event downstream
     * @param <OK> the output key type of the processor
     * @param <OV> the output value type of the processor
     * @param stageName the name of this stage
     * @param function the {@link BiFunction} to apply to the stream
     * @param flags optional {@link Flag} flags to dictate how the processing stage works
     */
    public <OV> PipelineBuilder<K, OV> apply(String stageName, BiFunction<K, V, OV> function, Flag... flags)
    {
        return apply(stageName, new ProcessorFunctionAdapter<K, V, OV>(function, isAutoCommit(flags)));
    }
    
    /**
     * Apply a {@link BiFunction} to this stream, which will take and event apply some kind of transformation and pass a new event downstream
     * @param <OK> the output key type of the processor
     * @param <OV> the output value type of the processor
     * @param function the {@link BiFunction} to apply to the stream
     * @param flags optional {@link Flag} flags to dictate how the processing stage works
     */
    public <OV> PipelineBuilder<K, OV> apply(BiFunction<K, V, OV> function, Flag... flags)
    {
        return apply(null, function, flags);
    }
    
    /**
     * Apply a {@link BiConsumer} to this stream, which will take and consume an event
     * @param stageName the name of this stage
     * @param consumer the {@link BiConsumer} to apply to the stream
     * @param flags optional {@link Flag} flags to dictate how the processing stage works
     */
    public PipelineBuilder<Void, Void> consume(String stageName, BiConsumer<K, V> consumer, Flag... flags)
    {
        return apply(stageName, new ProcessorConsumerAdapter<K, V>(consumer, isAutoCommit(flags)));
    }
    
    /**
     * Apply a {@link BiConsumer} to this stream, which will take and consume an event, auto-committing the event after being consumed
     * @param stageName the name of this stage
     * @param consumer the {@link BiConsumer} to apply to the stream
     */
    public PipelineBuilder<Void, Void> consume(String stageName, BiConsumer<K, V> consumer)
    {
        return consume(stageName, consumer, Flag.AUTO_COMMIT);
    }
    
    /**
     * Apply a {@link BiConsumer} to this stream, which will take and consume an event
     * @param consumer the {@link BiConsumer} to apply to the stream
     * @param flags optional {@link Flag} flags to dictate how the processing stage works
     */
    public PipelineBuilder<Void, Void> consume(BiConsumer<K, V> consumer, Flag... flags)
    {
        return consume(null, consumer, flags);
    }
    
    /**
     * Apply a {@link BiConsumer} to this stream, which will take and consume an event, auto-committing the event after being consumed
     * @param consumer the {@link BiConsumer} to apply to the stream
     */
    public PipelineBuilder<Void, Void> consume(BiConsumer<K, V> consumer)
    {
        return consume(null, consumer, Flag.AUTO_COMMIT);
    }
    
    // pipeline sink
    
    // register
    
    /**
     * Register this pipeline with the PGStreaming engine
     */
    public EngineBuilder register()
    {
        this.engine().registerPipeline(this);
        return this.engine();
    }

    // builder
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    PipelineExecutor<?,?> buildExecutor()
    {
        PGStreamingConnector<?, ?> connector = this.source().buildConnector();
        System.out.println("Connector: " + connector);
        List<Processor<?,?,?,?>> processors = this.processors();
        System.out.println("Processors: " + processors);
        return new PipelineExecutor(this.name(), connector, processors);
    }
    
}
