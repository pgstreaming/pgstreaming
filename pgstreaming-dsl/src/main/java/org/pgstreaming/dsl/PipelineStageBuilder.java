package org.pgstreaming.dsl;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public abstract class PipelineStageBuilder<IK, IV, OK, OV> extends PipelineBuilder<IK, IV>
{
    private final PipelineBuilder<IK, IV> parent;
    
    private final String stageName;

    protected PipelineStageBuilder(PipelineBuilder<IK, IV> parent, String stageName)
    {
        super();
        this.parent = Objects.requireNonNull(parent);
        this.stageName = (stageName == null) ? "stage-" + (parent.stage() + 1) : stageName;
    }
    
    @Override
    EngineBuilder engine()
    {
        return this.parent.engine();
    }

    @Override
    public final String name()
    {
        return this.parent.name();
    }

    @Override
    public String stageName()
    {
        return this.stageName;
    }

    @Override
    public int stage()
    {
        return this.parent.stage() + 1;
    }

    @Override
    public final SourceBuilder<?, ?> source()
    {
        return this.parent.source();
    }
    
    @Override
    public List<PipelineBuilder<?,?>> stages()
    {
        List<PipelineBuilder<?,?>> stages = this.parent.stages();
        stages.add(this);
        return stages;
    }
    
    public PipelineBuilder<IK, IV> parent()
    {
        return this.parent;
    }
    
    public String toString()
    {
        return this.name() + ":\n- " + this.stages().stream().map(PipelineBuilder::toStageString).collect(Collectors.joining("\n- "));
    }
    
}