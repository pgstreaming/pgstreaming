package org.pgstreaming.dsl;

public enum Flag
{
    /**
     * The processor should commit it's work automatically upon processing the event
     */
    AUTO_COMMIT
}