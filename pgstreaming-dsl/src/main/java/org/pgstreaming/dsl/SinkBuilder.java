package org.pgstreaming.dsl;

import java.util.Objects;

import org.pgstreaming.util.Require;

public class SinkBuilder<K, V>
{
    private final String name;
    
    private final Class<K> keyType;
    
    private final Class<V> valueType;
    
    public SinkBuilder(String name, Class<K> keyType, Class<V> valueType)
    {
        super();
        this.name = Require.nonEmpty(name);
        this.keyType = Objects.requireNonNull(keyType);
        this.valueType = Objects.requireNonNull(valueType);
    }
}
