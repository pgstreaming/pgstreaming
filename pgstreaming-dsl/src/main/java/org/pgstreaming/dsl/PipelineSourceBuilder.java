package org.pgstreaming.dsl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.pgstreaming.processor.Processor;
import org.pgstreaming.topology.executor.pipeline.PipelineExecutor;
import org.pgstreaming.util.Require;

public class PipelineSourceBuilder<K, V> extends PipelineBuilder<K, V>
{
    private final EngineBuilder engine;
    
    private final String pipelineName;
    
    private final SourceBuilder<K, V> source;
    
    PipelineSourceBuilder(EngineBuilder engine, String pipelineName, SourceBuilder<K, V> source)
    {
        super();
        this.engine = engine;
        this.pipelineName = Require.nonEmpty(pipelineName);
        this.source = Objects.requireNonNull(source);
    }
    
    @Override
    EngineBuilder engine()
    {
        return this.engine;
    }
    
    @Override
    public String name()
    {
        return this.pipelineName;
    }
    
    @Override
    public String stageName()
    {
        return "source(" + this.source.name() + ")";
    }

    @Override
    public int stage()
    {
        return 0;
    }

    @Override
    public SourceBuilder<K, V> source()
    {
        return this.source;
    }
    
    public List<PipelineBuilder<?,?>> stages()
    {
        List<PipelineBuilder<?,?>> stages = new ArrayList<>();
        stages.add(this);
        return stages;
    }
    
    @Override
    public List<Processor<?,?,?,?>> processors()
    {
        return new ArrayList<Processor<?,?,?,?>>();
    }
    
    @Override
    public String toStageString()
    {
        return this.stageName();
    }
    
    public String toString()
    {
        return this.pipelineName + ":\n- " + this.toStageString();
    }

    @Override
    PipelineExecutor<?, ?> buildExecutor()
    {
        throw new RuntimeException("A pipeline must have at least one processing or sink stage");
    }
}