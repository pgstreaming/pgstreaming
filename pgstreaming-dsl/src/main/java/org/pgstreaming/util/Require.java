package org.pgstreaming.util;

public final class Require
{
    private Require()
    {
    }
    
    public static final String nonEmpty(String s)
    {
        return nonEmpty(s, "A not empty string is required");
    }
    
    public static final String nonEmpty(String s, String message)
    {
        if (s == null || s.length() == 0 || s.trim().length() == 0)
            throw new IllegalArgumentException(message);
        return s;
    }
}
