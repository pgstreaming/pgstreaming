package org.pgstreaming.util;

import org.pgstreaming.dsl.Flag;

public final class FlagUtil
{
    private FlagUtil()
    {
    }
    
    public static boolean isAutoCommit(Flag... flags)
    {
        for (Flag flag : flags)
        {
            if (flag == Flag.AUTO_COMMIT)
                return true;
        }
        return false;
    }
}
