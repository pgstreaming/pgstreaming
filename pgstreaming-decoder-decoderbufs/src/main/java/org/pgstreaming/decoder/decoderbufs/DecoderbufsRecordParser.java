package org.pgstreaming.decoder.decoderbufs;

import java.nio.ByteBuffer;

import org.pgstreaming.metrics.PGStreamMetrics;
import org.pgstreaming.model.record.Record;
import org.pgstreaming.model.record.Schema;

import com.codahale.metrics.Timer;

import io.debezium.connector.postgresql.proto.PgProto.DatumMessage;
import io.debezium.connector.postgresql.proto.PgProto.Point;
import io.debezium.connector.postgresql.proto.PgProto.RowMessage;

public class DecoderbufsRecordParser extends DecoderbufsBaseParser<Record, Record>
{   
    protected final Timer buildKeyTimer = PGStreamMetrics.getRegistry().timer("pgstreaming.parser.decoderbufs.record.build_key");
    
    protected final Timer buildValueTimer = PGStreamMetrics.getRegistry().timer("pgstreaming.parser.decoderbufs.record.build_value");
    
    public DecoderbufsRecordParser()
    {
        super();
    }
        
    protected Schema buildKeySchema(RowMessage message)
    {
        // TODO
        return Schema.Cached.of(message.getTable() + "_key", () -> Schema.of(
            message.getTable() + "_key", 
            message.getTable()
        ));
    }
    
    protected Schema buildValueSchema(RowMessage message)
    {
        return Schema.Cached.of(message.getTable(), () -> Schema.of(
            message.getTable(), 
            message.getTable(), 
            message.getNewTupleList().stream()
            .map((DatumMessage c) -> Schema.of(c.getColumnName(), Object.class, "" + c.getColumnType()))
            .toArray(i -> new Schema[i])
        ));
    }
    
    protected static Schema datumSchema(DatumMessage datum)
    {
        switch (datum.getDatumCase())
        {
            case DATUM_BOOL:
                return Schema.of(datum.getColumnName(), Boolean.class, "boolean");
            case DATUM_BYTES:
                return Schema.of(datum.getColumnName(), ByteBuffer.class, "bytea");
            case DATUM_DOUBLE:
                return Schema.of(datum.getColumnName(), Double.class, "double");
            case DATUM_FLOAT:
                return Schema.of(datum.getColumnName(), Float.class, "float");
            case DATUM_INT32:
                return Schema.of(datum.getColumnName(), Integer.class, "integer");
            case DATUM_INT64:
                return Schema.of(datum.getColumnName(), Long.class, "bigint");
            case DATUM_POINT:
                return Schema.of(datum.getColumnName(), Point.class, "point");
            case DATUM_STRING:
                return Schema.of(datum.getColumnName(), String.class, "text");
            case DATUM_MISSING:
            case DATUM_NOT_SET:
            default:
                return Schema.of(datum.getColumnName(), Object.class, "");
        }
    }
    
    @Override
    protected Record buildValue(RowMessage message)
    {
        try (Timer.Context tctx = buildValueTimer.time())
        {
            Record value = new Record(this.buildValueSchema(message));
            for (DatumMessage datum : message.getNewTupleList())
            {
                switch (datum.getDatumCase())
                {
                    case DATUM_BOOL:
                        value.set(datum.getColumnName(), datum.getDatumBool());
                        break;
                    case DATUM_BYTES:
                        value.set(datum.getColumnName(), datum.getDatumBytes().asReadOnlyByteBuffer());
                        break;
                    case DATUM_DOUBLE:
                        value.set(datum.getColumnName(), datum.getDatumDouble());
                        break;
                    case DATUM_FLOAT:
                        value.set(datum.getColumnName(), datum.getDatumFloat());
                        break;
                    case DATUM_INT32:
                        value.set(datum.getColumnName(), datum.getDatumInt32());
                        break;
                    case DATUM_INT64:
                        value.set(datum.getColumnName(), datum.getDatumInt64());
                        break;
                    case DATUM_POINT:
                        value.set(datum.getColumnName(), datum.getDatumPoint());
                        break;
                    case DATUM_STRING:
                        value.set(datum.getColumnName(), datum.getDatumString());
                        break;
                    case DATUM_MISSING:
                    case DATUM_NOT_SET:
                    default:
                        value.set(datum.getColumnName(), null);
                        break;
                }
            }
            return value;
        }
    }
    
    @Override
    protected Record buildKey(RowMessage message)
    {
        try (Timer.Context tctx = buildKeyTimer.time())
        {
            Record value = new Record(this.buildKeySchema(message));
            // TODO
            return value;
        }
    }
}
