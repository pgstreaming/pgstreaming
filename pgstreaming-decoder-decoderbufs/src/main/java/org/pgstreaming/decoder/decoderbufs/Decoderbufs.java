package org.pgstreaming.decoder.decoderbufs;

import java.util.Collection;
import java.util.Collections;

import org.pgstreaming.decoder.DecoderPlugin;
import org.pgstreaming.decoder.DecoderPluginOutputParser;
import org.pgstreaming.model.record.Record;

public class Decoderbufs extends DecoderPlugin<Decoderbufs>
{

    public Decoderbufs()
    {
        super();
    }

    @Override
    public String name()
    {
        return "decoderbufs";
    }

    @Override
    public Collection<Option<?>> options()
    {
        return Collections.emptyList();
    }
    
    public Decoderbufs withIncludeTable(String... tables)
    {
        return this;
    }

    @Override
    protected DecoderPluginOutputParser<Record, Record> createRecordParser()
    {
        return new DecoderbufsRecordParser();
    }

    @Override
    protected <Key, Value> DecoderPluginOutputParser<Key, Value> createTypedParser(Class<Key> keyType, Class<Value> valueType)
    {
        return null;
    }
}
