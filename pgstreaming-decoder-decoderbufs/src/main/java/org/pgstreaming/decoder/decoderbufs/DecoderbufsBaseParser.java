package org.pgstreaming.decoder.decoderbufs;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.pgstreaming.decoder.DecoderPluginOutputParser;
import org.pgstreaming.metrics.PGStreamMetrics;
import org.pgstreaming.model.EventType;
import org.pgstreaming.model.PGStreamRecord;
import org.pgstreaming.model.StreamPosition;
import org.pgstreaming.model.impl.SimplePGStreamRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.Timer;

import io.debezium.connector.postgresql.proto.PgProto.Op;
import io.debezium.connector.postgresql.proto.PgProto.RowMessage;

public abstract class DecoderbufsBaseParser<K, V> implements DecoderPluginOutputParser<K, V>
{
    private static final Logger logger = LoggerFactory.getLogger(DecoderbufsBaseParser.class);
    
    protected final Timer protobufParseTimer = PGStreamMetrics.getRegistry().timer("pgstreaming.parser.decoderbufs.parse_protobuf");
    
    public DecoderbufsBaseParser()
    {
        super();
    }
    
    @Override
    public Collection<PGStreamRecord<K, V>> parse(ByteBuffer buffer, StreamPosition lsn)
    {
        try
        {
            // parse the message
            RowMessage message;
            try (Timer.Context tctx = protobufParseTimer.time())
            {
                message = RowMessage.parseFrom(buffer);
            }
            // filter out non-data events
            if (message.getOp() == Op.INSERT)
                return List.of(new SimplePGStreamRecord<>(lsn, message.getTable(), EventType.INSERT, buildKey(message), buildValue(message)));
            else if (message.getOp() == Op.UPDATE)
                return List.of(new SimplePGStreamRecord<>(lsn, message.getTable(), EventType.UPDATE, buildKey(message), buildValue(message)));
            else if (message.getOp() == Op.DELETE)
                return List.of(new SimplePGStreamRecord<>(lsn, message.getTable(), EventType.DELETE, buildKey(message), buildValue(message)));
            else if (message.getOp() == Op.BEGIN)
                return List.of(new SimplePGStreamRecord<>(lsn, null, EventType.BEGIN, null, null));
            else if (message.getOp() == Op.COMMIT)
                return List.of(new SimplePGStreamRecord<>(lsn, null, EventType.COMMIT, null, null));
            // default
            return List.of(new SimplePGStreamRecord<>(lsn, null, null, null, null));
        }
        catch (IOException e)
        {
            logger.error("Failed to decode logical message", e);
        }
        return Collections.emptyList();
    }
    
    protected abstract V buildValue(RowMessage message);
    
    protected abstract K buildKey(RowMessage message);
}
