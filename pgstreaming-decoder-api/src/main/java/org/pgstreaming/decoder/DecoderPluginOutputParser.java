package org.pgstreaming.decoder;

import java.nio.ByteBuffer;
import java.util.Collection;

import org.pgstreaming.model.PGStreamRecord;
import org.pgstreaming.model.StreamPosition;

public interface DecoderPluginOutputParser<K, V>
{
    Collection<PGStreamRecord<K,V>> parse(ByteBuffer buffer, StreamPosition position);
}
