package org.pgstreaming.decoder;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.stream.Collectors;

import org.pgstreaming.model.record.Record;

public abstract class DecoderPlugin<T extends DecoderPlugin<T>>
{    
    protected final Map<Option<?>, Object> slotOptions = new HashMap<>();
    
    public DecoderPlugin()
    {
        super();
        // setup defaults
        for (Option<?> option : this.options())
        {
            if (option.defaultValue() != null)
            {
                this.slotOptions.put(option, option.defaultValue());
            }
        }
    }
    
    public abstract String name();
    
    public abstract Collection<Option<?>> options();
    
    public Map<Option<?>, Object> slotOptions()
    {
        return this.slotOptions;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public Properties configureSlot()
    {
        Properties props = new Properties();
        for (Entry<Option<?>, Object> slotOption : this.slotOptions.entrySet())
        {
            ((Option) slotOption.getKey()).configureSlot(props, slotOption.getValue());
        }
        return props;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public <Key, Value> DecoderPluginOutputParser<Key, Value> createParser(Class<Key> keyType, Class<Value> valueType)
    {
        return (keyType == Record.class && valueType == Record.class) ? (DecoderPluginOutputParser) this.createRecordParser() : this.createTypedParser(keyType, valueType);
    }
    
    protected abstract DecoderPluginOutputParser<Record, Record> createRecordParser();
    
    protected abstract <Key, Value> DecoderPluginOutputParser<Key, Value> createTypedParser(Class<Key> keyType, Class<Value> valueType);
    
    @SuppressWarnings("unchecked")
    public <V> T with(Option<V> option, V value)
    {
        if (value != null)
        {
            // validate the value
            option.validateOptionValue(value);
            // set the option
            this.slotOptions.put(option, value);
        }
        else
        {
            // unset the option
            this.slotOptions.remove(option);
        }
        return (T) this;
    }
    
    @SuppressWarnings("unchecked")
    public T with(StringOption option, Collection<String> values)
    {
        if (values != null && ! values.isEmpty())
        {
            // validate the value
            for (String value : values)
            {
                option.validateOptionValue(value);
            }
            // set the option
            this.slotOptions.put(option, values.stream().collect(Collectors.joining(",")));
        }
        else
        {
            // unset the option
            this.slotOptions.remove(option);
        }
        return (T) this;
    }
    
    public T with(StringOption option, String... values)
    {
        return this.with(option, List.of(values));
    }
    
    public abstract T withIncludeTable(String... tables);
    
    public static abstract class Option<T>
    {
        private final String name;
        
        private final T[] values;
        
        private final T defaultValue;
        
        protected Option(String name, T[] values, T defaultValue)
        {
            super();
            this.name = name;
            this.values = values;
            this.defaultValue = defaultValue;
        }
        
        public final String optionName()
        {
            return this.name;
        }
        
        public final T[] optionValues()
        {
            return this.values;
        }
        
        public final T defaultValue()
        {
            return this.defaultValue;
        }
        
        public void validateOptionValue(T value)
        {
            T[] allowedValues = this.optionValues();
            if (allowedValues == null || allowedValues.length == 0) 
                return;
            for (T allowedValue : allowedValues)
            {
                if (allowedValue != null && allowedValue.equals(value))
                    return;
            }
            throw new IllegalArgumentException("The value '" + value + "' is not valid for the option '" + this.optionName() + "'");
        }
        
        public final String toString()
        {
            return this.optionName() + " = " + (this.defaultValue() == null ? "" : this.defaultValue());
        }
        
        @Override
        public int hashCode()
        {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((name == null) ? 0 : name.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj)
        {
            if (this == obj) return true;
            if (obj == null) return false;
            if (getClass() != obj.getClass()) return false;
            Option<?> other = (Option<?>) obj;
            if (name == null)
            {
                if (other.name != null) return false;
            }
            else if (!name.equals(other.name)) return false;
            return true;
        }

        public abstract void configureSlot(Properties slot, T value);
    }
    
    public static final class BooleanOption extends Option<Boolean>
    {        
        public BooleanOption(String name, boolean defaultValue)
        {
            super(name, new Boolean[] { Boolean.TRUE, Boolean.FALSE }, defaultValue);
        }
        
        public void configureSlot(Properties slot, Boolean value)
        {
            slot.setProperty(this.optionName(), value.toString());
        }
    }
    
    public static final class IntOption extends Option<Integer>
    {        
        public IntOption(String name, int defaultValue)
        {
            super(name, null, defaultValue);
        }
        
        public IntOption(String name)
        {
            super(name, null, null);
        }
        
        public void configureSlot(Properties slot, Integer value)
        {
            slot.setProperty(this.optionName(), value.toString());
        }
    }
    
    public static final class StringOption extends Option<String>
    {
        public StringOption(String name, String[] options, String defaultValue)
        {
            super(name, options, defaultValue);
        }
        
        public StringOption(String name, String[] options)
        {
            super(name, options, null);
        }
        
        public StringOption(String name, String defaultValue)
        {
            super(name, null, defaultValue);
        }
        
        public StringOption(String name)
        {
            super(name, null, null);
        }
        
        public void configureSlot(Properties slot, String value)
        {
            slot.setProperty(this.optionName(), value);
        }
    }
    
    public final String toString()
    {
        return this.name() + " WITH " + this.slotOptions().entrySet().stream().map((k) -> k.getKey() + "=" + k.getValue()).collect(Collectors.joining(", "));
    }
    
    protected static Collection<Option<?>> findOptions(Class<? extends DecoderPlugin<?>> from)
    {
        List<Option<?>> options = new ArrayList<>();
        for (Field field : from.getFields())
        {
            if (Modifier.isStatic(field.getModifiers()) && Option.class.isAssignableFrom(field.getType()))
            {
                try
                {
                    options.add((Option<?>) field.get(null));
                }
                catch (IllegalArgumentException | IllegalAccessException e)
                {
                }
            }
        }
        return Collections.unmodifiableList(options);
    }
}
