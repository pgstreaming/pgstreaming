package org.pgstreaming.mapper.converter;

public interface ValueConverter<T>
{
    T read(Object value);
}
