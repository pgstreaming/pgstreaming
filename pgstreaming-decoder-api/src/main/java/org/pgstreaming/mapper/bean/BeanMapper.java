package org.pgstreaming.mapper.bean;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.pgstreaming.mapper.FieldValue;
import org.pgstreaming.mapper.TypeMapper;
import org.pgstreaming.mapper.annotation.Column;
import org.pgstreaming.mapper.annotation.Primary;
import org.pgstreaming.metrics.PGStreamMetrics;

import com.codahale.metrics.Timer;

public final class BeanMapper<T> implements TypeMapper<T>
{
    private final Class<T> type;
    
    private final Map<String, FieldBinder> bindings = new HashMap<>();
    
    private final Timer bindTimer;
    
    public BeanMapper(Class<T> type, boolean key)
    {
        super();
        this.type = Objects.requireNonNull(type);
        // metrics
        this.bindTimer = PGStreamMetrics.getRegistry().timer("pgstream.mapper.bean.bind[class=" + type.getCanonicalName() + "]");
        // introspect the bean and build the binders
        buildFieldBinders(this.type, key, this.bindings);
    }
    
    private static void buildFieldBinders(Class<?> type, boolean key, Map<String, FieldBinder> bindings)
    {
        if (type == null)
            return;
        // go up the chain
        buildFieldBinders(type.getSuperclass(), key, bindings);
        // process the fields for this class
        for (Field field : type.getDeclaredFields())
        {
            Column col = field.getAnnotation(Column.class);
            Primary pri = field.getAnnotation(Primary.class);
            if (col != null && (! key || pri != null))
            {
                field.setAccessible(true);
                bindings.put(col.value(), new FieldBinder(field));
            }
        }
    }
    
    @Override
    public Class<T> type()
    {
        return this.type;
    }
    
    @Override
    public T bind(Collection<? extends FieldValue> record)
    {
        try (Timer.Context tctx = this.bindTimer.time())
        {
        try
        {
            T value = this.type.getDeclaredConstructor().newInstance();
            for (FieldValue fieldValue : record)
            {
                FieldBinder binder = this.bindings.get(fieldValue.getName());
                if (binder != null)
                    binder.set(value, fieldValue.getValue());
            }
            return value;
        }
        catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e)
        {
            e.printStackTrace();
        }
        }
        return null;
    }

    private static class FieldBinder
    {
        private final Field field;
        
        public FieldBinder(Field field)
        {
            this.field = field;
        }
        
        public void set(Object on, Object value) throws IllegalArgumentException, IllegalAccessException
        {
            // TODO: convert value
            field.set(on, value);
        }
        
        public String toString()
        {
            return field.getName();
        }
    }
}
