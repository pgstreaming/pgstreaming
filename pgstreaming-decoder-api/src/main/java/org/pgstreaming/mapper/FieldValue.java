package org.pgstreaming.mapper;

public interface FieldValue
{
    String getName();
    
    String getType();
    
    Object getValue();
}
