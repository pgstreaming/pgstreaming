package org.pgstreaming.mapper;

import java.util.Collection;

public interface TypeMapper<T>
{
    Class<T> type();
    
    T bind(Collection<? extends FieldValue> record);
}