package org.pgstreaming.topology.executor.pipeline;

import java.util.Objects;

import org.pgstreaming.model.PGStreamRecordMeta;
import org.pgstreaming.model.StreamPosition;
import org.pgstreaming.processor.Processor;
import org.pgstreaming.processor.ProcessorContext;

/**
 * An execution stage within a processing pipeline.
 *
 * @param <IK> the input key type
 * @param <IV> the input value type
 * @param <OK> the output key type
 * @param <OV> the output value type
 */
public class ProcessorChain<IK, IV, OK, OV>
{
    private final PipelineContext pipelineContext;
    
    private final Processor<IK, IV, OK, OV> processor;
    
    private final ProcessorChain<OK, OV, ?, ?> next;
    
    private final ProcessorContext<IK, IV, OK, OV> context;
    
    public ProcessorChain(PipelineContext pipelineContext, Processor<IK, IV, OK, OV> processor, ProcessorChain<OK, OV, ?, ?> next)
    {
        super();
        this.pipelineContext = Objects.requireNonNull(pipelineContext);
        this.processor = Objects.requireNonNull(processor);
        this.next = next;
        this.context = new ProcessorContext<IK, IV, OK, OV>() {
            @Override
            public void forward(PGStreamRecordMeta meta, OK key, OV value)
            {
                // forward down the chain
                if (ProcessorChain.this.next != null)
                    ProcessorChain.this.next.process(meta, key, value);
            }

            @Override
            public void commit(StreamPosition position)
            {
                ProcessorChain.this.pipelineContext.commit(position);
            }
        };
    }
    
    /**
     * Init the processor execution chain
     */
    public void init()
    {
        // init the process
        this.processor.init(this.context);
        // init the chain
        if (this.next != null)
            this.next.init();
    }
    
    /**
     * Process the event
     * @param meta the event metadata
     * @param key the event key
     * @param value the event value
     */
    public void process(PGStreamRecordMeta meta, IK key, IV value)
    {
        // invoke the processor
        this.processor.process(this.context, meta, key, value);
    }
}
