package org.pgstreaming.topology.executor.pipeline;

import org.pgstreaming.model.StreamPosition;

/**
 * The execution context of the whole pipeline
 */
public interface PipelineContext
{
    void commit(StreamPosition position);
}
