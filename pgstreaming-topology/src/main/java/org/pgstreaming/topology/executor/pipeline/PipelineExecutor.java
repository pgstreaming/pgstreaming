package org.pgstreaming.topology.executor.pipeline;

import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

import org.pgstreaming.PGStreamingConnector;
import org.pgstreaming.model.StreamPosition;
import org.pgstreaming.processor.Processor;

/**
 * A very basic pipeline executor
 */
public class PipelineExecutor<K, V>
{
    private final String name;
    
    private final PGStreamingConnector<K, V> connector;
    
    private final PipelineContext context;
    
    private final ProcessorChain<K, V, ?, ?> processorChain;
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public PipelineExecutor(String name, PGStreamingConnector<K, V> connector, List<Processor<?, ?, ?, ?>> processors)
    {
        super();
        this.name = Objects.requireNonNull(name);
        this.connector = Objects.requireNonNull(connector);
        this.context = new PipelineContext() {
            @Override
            public void commit(StreamPosition position)
            {
                try
                {
                    PipelineExecutor.this.connector.commit(position);
                }
                catch (Exception e)
                {
                    throw new RuntimeException(e);
                }
            }
        };
        // setup the processor chain
        ProcessorChain<?,?,?,?> next = null;
        for (ListIterator<Processor<?, ?, ?, ?>> i = processors.listIterator(processors.size()); i.hasPrevious();)
        {
            Processor<?, ?, ?, ?> processor = i.previous();
            next = new ProcessorChain(this.context, processor, next);
        }
        this.processorChain = (ProcessorChain<K, V, ?, ?>) next;
    }
    
    public String name()
    {
        return this.name;
    }
    
    /**
     * Setup this pipeline
     */
    public void init()
    {
        // init the processing pipeline
        this.processorChain.init();   
    }
    
    /**
     * Run this pipeline
     */
    public void run()
    {
        try
        {
            // start the connector
            this.connector.connect();
            // consume event
            this.connector.consume((record) -> {
                this.processorChain.process(record, record.key(), record.value());
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    
    /**
     * Start this pipeline in a new thread
     */
    public void start()
    {
        new Thread(this::run, "pgstreaming-pipeline-main-" + this.name).start();
    }
    
    /**
     * Shutdown this pipeline
     */
    public void shutdown()
    {
        this.connector.close();
    }
}
