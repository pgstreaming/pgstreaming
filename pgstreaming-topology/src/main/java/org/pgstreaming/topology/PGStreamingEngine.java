package org.pgstreaming.topology;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import org.pgstreaming.topology.executor.pipeline.PipelineExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A super basic topology execution engine
 */
public class PGStreamingEngine
{
    public static final Logger logger = LoggerFactory.getLogger(PGStreamingEngine.class);
    
    private final String name;
    
    private final Map<String, PipelineExecutor<?,?>> pipelines = new ConcurrentHashMap<>();
    
    public PGStreamingEngine(String name)
    {
        super();
        this.name = Objects.requireNonNull(name);
    }
    
    public String name()
    {
        return this.name;
    }
    
    public void registerPipeline(PipelineExecutor<?,?> pipeline)
    {
        this.pipelines.put(pipeline.name(), pipeline);
    }
    
    // lifecycle
    
    public void init()
    {
        // init pipelines
        for (PipelineExecutor<?,?> pipeline : this.pipelines.values())
        {
            logger.info("Initing pipeline {}\n{}", pipeline.name(), pipeline);
            pipeline.init();
        }
    }
    
    public void start()
    {
        for (PipelineExecutor<?,?> pipeline : this.pipelines.values())
        {
            logger.info("Starting pipeline {}\n{}", pipeline.name(), pipeline);
            pipeline.start();
        }
    }
    
    public void shutdown()
    {
        for (PipelineExecutor<?,?> pipeline : this.pipelines.values())
        {
            logger.info("Shuttingdown pipeline {}\n{}", pipeline.name(), pipeline);
            pipeline.shutdown();
        }
    }
}
