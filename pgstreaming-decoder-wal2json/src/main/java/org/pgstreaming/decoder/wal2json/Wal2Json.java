package org.pgstreaming.decoder.wal2json;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

import org.pgstreaming.decoder.DecoderPlugin;
import org.pgstreaming.decoder.DecoderPluginOutputParser;
import org.pgstreaming.model.record.Record;

public class Wal2Json extends DecoderPlugin<Wal2Json>
{
    public static final BooleanOption INCLUDE_XIDS = new BooleanOption("include-xids", false);
    public static final BooleanOption INCLUDE_TIMESTAMP = new BooleanOption("include-timestamp", false);
    public static final BooleanOption INCLUDE_SCHEMAS = new BooleanOption("include-schemas", true);
    public static final BooleanOption INCLUDE_TYPES = new BooleanOption("include-types", true);
    public static final BooleanOption INCLUDE_TYPMOD = new BooleanOption("include-typmod", true);
    public static final BooleanOption INCLUDE_TYPE_OIDS = new BooleanOption("include-type-oids", false);
    public static final BooleanOption INCLUDE_NOT_NULL = new BooleanOption("include-not-null", false);
    public static final BooleanOption INCLUDE_PK = new BooleanOption("include-pk", true);
    public static final BooleanOption PRETTY_PRINT = new BooleanOption("pretty-print", false);
    public static final BooleanOption WRITE_IN_CHUNKS = new BooleanOption("write-in-chunks", false);
    public static final BooleanOption INCLUDE_LSN = new BooleanOption("include-lsn", true);
    public static final BooleanOption INCLUDE_TRANSACTION = new BooleanOption("include-transaction", true);
    public static final StringOption FILTER_TABLES = new StringOption("filter-tables");
    public static final StringOption ADD_TABLES = new StringOption("add-tables");
    public static final IntOption FORMAT_VERSION = new IntOption("format-version", 2);

    private static final String NAME = "wal2json";
    private static final Collection<Option<?>> OPTIONS = findOptions(Wal2Json.class);

    public Wal2Json()
    {
        super();
    }

    @Override
    public String name()
    {
        return NAME;
    }

    @Override
    public Collection<Option<?>> options()
    {
        return OPTIONS;
    }
    
    public Wal2Json withIncludeTable(String... tables)
    {
        this.with(ADD_TABLES, Arrays.stream(tables).collect(Collectors.joining(",")));
        return this;
    }
    
    @Override
    protected DecoderPluginOutputParser<Record, Record> createRecordParser()
    {
        return new Wal2JsonRecordParser();
    }

    @Override
    protected <Key, Value> DecoderPluginOutputParser<Key, Value> createTypedParser(Class<Key> keyType, Class<Value> valueType)
    {
        return new Wal2JsonTypedParser<>(keyType, valueType);
    }
}
