package org.pgstreaming.decoder.wal2json.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class WALMessage
{
    public enum Action {
        M,
        B,
        I,
        D,
        U,
        C,
        T;
    }
    
    @JsonProperty("action")
    private Action action;
    
    @JsonProperty("xid")
    private long xid;
    
    @JsonProperty("timestamp")
    private String timestamp;
    
    @JsonProperty("schema")
    private String schema;
    
    @JsonProperty("table")
    private String table;
    
    @JsonProperty("identity")
    private List<WALColumnValue> identity = new ArrayList<>();
    
    @JsonProperty("columns")
    private List<WALColumnValue> columns = new ArrayList<>();
    
    @JsonProperty("pk")
    private List<WALColumn> primaryKey = new ArrayList<>();
    
    public WALMessage()
    {
        super();
    }

    public Action getAction()
    {
        return this.action;
    }

    public void setAction(Action action)
    {
        this.action = action;
    }

    public long getXid()
    {
        return this.xid;
    }

    public void setXid(long xid)
    {
        this.xid = xid;
    }

    public String getTimestamp()
    {
        return this.timestamp;
    }

    public void setTimestamp(String timestamp)
    {
        this.timestamp = timestamp;
    }

    public String getSchema()
    {
        return this.schema;
    }

    public void setSchema(String schema)
    {
        this.schema = schema;
    }

    public String getTable()
    {
        return this.table;
    }

    public void setTable(String table)
    {
        this.table = table;
    }
    
    @JsonIgnore
    public String getTableFQN()
    {
        return this.schema + "." + this.table;
    }

    public List<WALColumnValue> getIdentity()
    {
        return this.identity;
    }

    public void setIdentity(List<WALColumnValue> identity)
    {
        this.identity = identity;
    }

    public List<WALColumnValue> getColumns()
    {
        return this.columns;
    }

    public void setColumns(List<WALColumnValue> columns)
    {
        this.columns = columns;
    }
    
    public List<WALColumn> getPrimaryKey()
    {
        return this.primaryKey;
    }

    public void setPrimaryKey(List<WALColumn> primaryKey)
    {
        this.primaryKey = primaryKey;
    }

    public String toString()
    {
        return this.xid + " " + this.action + " " + this.schema + "." + this.table + " " + this.identity + " " + this.columns + " " + this.primaryKey;
    }
}
