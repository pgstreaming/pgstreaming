package org.pgstreaming.decoder.wal2json;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.pgstreaming.decoder.DecoderPluginOutputParser;
import org.pgstreaming.decoder.wal2json.model.WALMessage;
import org.pgstreaming.decoder.wal2json.model.WALMessage.Action;
import org.pgstreaming.metrics.PGStreamMetrics;
import org.pgstreaming.model.EventType;
import org.pgstreaming.model.PGStreamRecord;
import org.pgstreaming.model.StreamPosition;
import org.pgstreaming.model.impl.SimplePGStreamRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.Timer;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class Wal2JsonBaseParser<K, V> implements DecoderPluginOutputParser<K, V>
{
    private static final Logger logger = LoggerFactory.getLogger(Wal2JsonBaseParser.class);
    
    protected final ObjectMapper objectMapper;
    
    protected final Timer jsonParseTimer = PGStreamMetrics.getRegistry().timer("pgstreaming.parser.wal2json.parse_json");
    
    public Wal2JsonBaseParser()
    {
        super();
        // setup our JSON parser
        this.objectMapper = new ObjectMapper();
        this.objectMapper.setSerializationInclusion(Include.NON_NULL);
        this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }
    
    @Override
    public Collection<PGStreamRecord<K, V>> parse(ByteBuffer buffer, StreamPosition position)
    {
        try
        {
            // parse the message
            WALMessage message;
            try (Timer.Context tctx = jsonParseTimer.time())
            {
                message = this.objectMapper.readValue(buffer.array(), buffer.arrayOffset(), buffer.remaining(), WALMessage.class);
            }
            // filter out non-data events
            if (message.getAction() == Action.I)
                return List.of(new SimplePGStreamRecord<>(position, message.getSchema() + "." + message.getTable(), EventType.INSERT, buildKey(message), buildValue(message)));
            else if (message.getAction() == Action.U)
                return List.of(new SimplePGStreamRecord<>(position, message.getSchema() + "." + message.getTable(), EventType.UPDATE, buildKey(message), buildValue(message)));
            else if (message.getAction() == Action.D)
                return List.of(new SimplePGStreamRecord<>(position, message.getSchema() + "." + message.getTable(), EventType.DELETE, buildKey(message), buildValue(message)));
            else if (message.getAction() == Action.B)
                return List.of(new SimplePGStreamRecord<>(position, null, EventType.BEGIN, null, null));
            else if (message.getAction() == Action.C)
                return List.of(new SimplePGStreamRecord<>(position, null, EventType.COMMIT, null, null));
            else
            return List.of(new SimplePGStreamRecord<>(position, null, null, null, null));
        }
        catch (IOException e)
        {
            logger.error("Failed to decode logical message", e);
        }
        return Collections.emptyList();
    }
    
    protected abstract V buildValue(WALMessage message);
    
    protected abstract K buildKey(WALMessage message);
}
