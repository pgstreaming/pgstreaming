package org.pgstreaming.decoder.wal2json;

import org.pgstreaming.decoder.wal2json.model.WALColumnValue;
import org.pgstreaming.decoder.wal2json.model.WALMessage;
import org.pgstreaming.metrics.PGStreamMetrics;
import org.pgstreaming.model.record.Record;
import org.pgstreaming.model.record.Schema;

import com.codahale.metrics.Timer;

public class Wal2JsonRecordParser extends Wal2JsonBaseParser<Record, Record>
{   
    protected final Timer buildKeyTimer = PGStreamMetrics.getRegistry().timer("pgstreaming.parser.wal2json.record.build_key");
    
    protected final Timer buildValueTimer = PGStreamMetrics.getRegistry().timer("pgstreaming.parser.wal2json.record.build_value");
    
    public Wal2JsonRecordParser()
    {
        super();
    }
        
    protected Schema buildKeySchema(WALMessage message)
    {
        return Schema.Cached.of(message.getTableFQN() + "_key", () -> Schema.of(
            message.getTableFQN() + "_key", 
            message.getTableFQN(), 
            message.getPrimaryKey().stream()
                .map(c -> Schema.of(c.getName(), Object.class, c.getType()))
                .toArray(i -> new Schema[i])
        ));
    }
    
    protected Schema buildValueSchema(WALMessage message)
    {
        return Schema.Cached.of(message.getTableFQN(), () -> Schema.of(
            message.getTableFQN(), 
            message.getTableFQN(), 
            message.getColumns().stream()
                .map(c -> Schema.of(c.getName(), Object.class, c.getType()))
                .toArray(i -> new Schema[i])
        ));
    }
    
    @Override
    protected Record buildValue(WALMessage message)
    {
        try (Timer.Context tctx = buildValueTimer.time())
        {
            Record value = new Record(this.buildValueSchema(message));
            for (WALColumnValue colVal : message.getColumns())
            {
                value.set(colVal.getName(), colVal.getValue());
            }
            return value;
        }
    }
    
    @Override
    protected Record buildKey(WALMessage message)
    {
        try (Timer.Context tctx = buildKeyTimer.time())
        {
            Record value = new Record(this.buildKeySchema(message));
            if (! message.getColumns().isEmpty())
            {
                for (WALColumnValue colVal : message.getColumns())
                {
                    int index = value.schema().fieldIndex(colVal.getName());
                    if (index >= 0)
                        value.set(index, colVal.getValue());
                }
            }
            else if (! message.getIdentity().isEmpty())
            {
                for (WALColumnValue colVal : message.getIdentity())
                {
                    value.set(colVal.getName(), colVal.getValue());
                }
            }
            return value;
        }
    }
}
