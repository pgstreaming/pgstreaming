package org.pgstreaming.decoder.wal2json.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WALColumn
{
    @JsonProperty("name")
    private String name;
    
    @JsonProperty("type")
    private String type;
    
    public WALColumn()
    {
        super();
    }

    public WALColumn(String name, String type)
    {
        super();
        this.name = name;
        this.type = type;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getType()
    {
        return this.type;
    }

    public void setType(String type)
    {
        this.type = type;
    }
    
    public String toString()
    {
        return this.name + "[" + this.type + "]";
    }
}
