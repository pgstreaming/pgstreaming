package org.pgstreaming.decoder.wal2json.model;

import org.pgstreaming.mapper.FieldValue;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WALColumnValue extends WALColumn implements FieldValue
{
    @JsonProperty("value")
    private Object value;

    public WALColumnValue()
    {
        super();
    }

    public WALColumnValue(String name, String type, Object value)
    {
        super(name, type);
        this.value = value;
    }

    public Object getValue()
    {
        return this.value;
    }

    public void setValue(Object value)
    {
        this.value = value;
    }

    public String toString()
    {
        return super.toString() + "=" + this.value;
    }
}
