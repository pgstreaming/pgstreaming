package org.pgstreaming.decoder.wal2json;

import org.pgstreaming.decoder.wal2json.model.WALMessage;
import org.pgstreaming.mapper.TypeMapper;
import org.pgstreaming.mapper.bean.BeanMapper;
import org.pgstreaming.metrics.PGStreamMetrics;

import com.codahale.metrics.Timer;

public class Wal2JsonTypedParser<K, V> extends Wal2JsonBaseParser<K, V>
{   
    private final TypeMapper<K> keyMapper;
    
    private final TypeMapper<V> valueMapper;
    
    protected final Timer buildKeyTimer;
    
    protected final Timer buildValueTimer;
    
    public Wal2JsonTypedParser(Class<K> keyType, Class<V> valueType)
    {
        super();
        this.buildKeyTimer   = PGStreamMetrics.getRegistry().timer("pgstreaming.parser.wal2json.typed.build_key[class=" + keyType.getCanonicalName() + "]");
        this.buildValueTimer = PGStreamMetrics.getRegistry().timer("pgstreaming.parser.wal2json.typed.build_value[class=" + valueType.getCanonicalName() + "]");
        this.keyMapper = new BeanMapper<>(keyType, true);
        this.valueMapper = new BeanMapper<>(valueType, false);
    }
    
    @Override
    protected V buildValue(WALMessage message)
    {
        try (Timer.Context tctx = this.buildKeyTimer.time())
        {
            return this.valueMapper.bind(message.getColumns());
        }
    }
    
    @Override
    protected K buildKey(WALMessage message)
    {
        try (Timer.Context tctx = this.buildValueTimer.time())
        {
            if (! message.getColumns().isEmpty())
                return this.keyMapper.bind(message.getColumns());
            else if (! message.getIdentity().isEmpty())
                return this.keyMapper.bind(message.getIdentity());
            return null;
        }
    }
}
