package org.pgstreaming.processor.adapter;

import java.util.Objects;
import java.util.function.BiFunction;

import org.pgstreaming.model.PGStreamRecordMeta;
import org.pgstreaming.processor.Processor;
import org.pgstreaming.processor.ProcessorContext;

public class ProcessorFunctionAdapter<K, IV, OV> implements Processor<K, IV, K, OV>
{
    private final BiFunction<K, IV, OV> function;
    
    private final boolean autoCommit;
    
    public ProcessorFunctionAdapter(BiFunction<K, IV, OV> function,  boolean autoCommit)
    {
        super();
        this.function = Objects.requireNonNull(function);
        this.autoCommit = autoCommit;
    }
    
    public ProcessorFunctionAdapter(BiFunction<K, IV, OV> function)
    {
        this(function, false);
    }

    @Override
    public void process(ProcessorContext<K, IV, K, OV> context, PGStreamRecordMeta meta, K key, IV value)
    {
        // apply the function
        OV output = this.function.apply(key, value);
        // should we commit
        if (this.autoCommit)
            context.commit(meta);
        // forward
        context.forward(meta, key, output);
    }
    
    public String toString()
    {
        return "ProcessorFunctionAdapter(" + this.function.toString() + ")";
    }
}
