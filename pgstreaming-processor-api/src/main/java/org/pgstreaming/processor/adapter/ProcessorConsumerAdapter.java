package org.pgstreaming.processor.adapter;

import java.util.Objects;
import java.util.function.BiConsumer;

import org.pgstreaming.model.PGStreamRecordMeta;
import org.pgstreaming.processor.Processor;
import org.pgstreaming.processor.ProcessorContext;

public class ProcessorConsumerAdapter<K, V> implements Processor<K, V, Void, Void>
{
    private final BiConsumer<K, V> consumer;
    
    private final boolean autoCommit;
    
    public ProcessorConsumerAdapter(BiConsumer<K, V> consumer,  boolean autoCommit)
    {
        super();
        this.consumer = Objects.requireNonNull(consumer);
        this.autoCommit = autoCommit;
    }
    
    public ProcessorConsumerAdapter(BiConsumer<K, V> consumer)
    {
        this(consumer, false);
    }

    @Override
    public void process(ProcessorContext<K, V, Void, Void> context, PGStreamRecordMeta meta, K key, V value)
    {
        // call the consumer
        this.consumer.accept(key, value);
        // should we commit
        if (this.autoCommit)
            context.commit(meta);
    }
    
    public String toString()
    {
        return "ProcessorConsumerAdapter(" + this.consumer.toString() + ")";
    }
}
