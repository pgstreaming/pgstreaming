package org.pgstreaming.processor;

import org.pgstreaming.model.PGStreamRecordMeta;
import org.pgstreaming.model.StreamPosition;

/**
 * The context of a Processor, used to access the next processor of the pipeline and the wider PGStreaming framework.
 *
 * @param <IK> the input key type
 * @param <IV> the input value type
 * @param <OK> the output key type
 * @param <OV> the output value type
 */
public interface ProcessorContext<IK, IV, OK, OV>
{
    // forward
    
    /**
     * Forward the given event to the next processor or sink in the pipeline
     * 
     * @param meta the event metadata
     * @param key the event key
     * @param value the event value
     */
    void forward(PGStreamRecordMeta meta, OK key, OV value);
    
    // commit
    
    /**
     * Commit this position to the stream source
     * @param position the event to commit
     */
    void commit(StreamPosition position);
    
    /**
     * Commit the position of the given record to stream source
     * @param meta the event to commit
     */
    default void commit(PGStreamRecordMeta meta)
    {
        this.commit(meta.position());
    }
}
