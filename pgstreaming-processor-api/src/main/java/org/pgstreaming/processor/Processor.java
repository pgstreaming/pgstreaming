package org.pgstreaming.processor;

import org.pgstreaming.model.PGStreamRecordMeta;

/**
 * A processing step within a PGStreaming stream.
 * 
 * A processor takes an input record and performs some computation, optionally forwarding a new value down the streaming pipeline.
 *
 * @param <IK> the input key type
 * @param <IV> the input value type
 * @param <OK> the output key type
 * @param <OV> the output value type
 */
@FunctionalInterface
public interface Processor<IK, IV, OK, OV>
{
    /**
     * Setup this processor
     * @param context the context for this processor, which is valid for the lifetime of the processor and can be stored in a member field
     */
    default void init(ProcessorContext<IK, IV, OK, OV> context)
    {
    }
    
    /**
     * Process the given stream event.
     * 
     * To output an event to the next stage of a pipeline use {@link ProcessorContext.forward}
     * 
     * @param context the context of this processor
     * @param meta the event metadata
     * @param key the event key
     * @param value the event value
     */
    void process(ProcessorContext<IK, IV, OK, OV> context, PGStreamRecordMeta meta, IK key, IV value);
}
