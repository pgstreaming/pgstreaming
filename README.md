# PGStreaming - Data Streaming Applications For PostgreSQL 

PGStreaming is a simple Java framework  which aims to make building data streaming application with PostgreSQL easier.

PGStreaming enables you to build data streaming applications directly against PostgreSQL by utilising PostgreSQLs Logical Decoding (the underpinnings of Logical Replication) support to stream data events directly from the PostgreSQL WAL.

## Status

At the moment, this project is very much an alhpa, the base concept has been demonstrated and it under active development.

## Example

```
import static org.pgstreaming.PGStreaming.*;

import org.pgstreaming.model.record.Record;

import example.home_insurance.service.EmailService;

public class HIEmailDSLDemo
{
    public static void main(String[] args) throws Exception
    {
        // enable metrics logging
        // PGStreamMetrics.enableCSVReporter();
        // email service
        EmailService emailer = new EmailService();
        // consume the stream
        pgStreaming()
        .source()
        .name("policies")
        .ofRecord()
        .from("jdbc:postgresql://127.0.0.1:5414/streaming", "rep", "")
        .slot("welcome_email")
        .wal2json()
        .includeTable("event.policy")
        .then()
        .consume((Record key, Record value) -> {
            emailer.sendWelcomeEmail(
                    value.get("customer_email"), 
                    value.get("customer_name"), 
                    value.get("policy_reference"), 
                    value.get("post_code")
            );
        })
        .register()
        .launch();
    }
}
```

## Licence

Copyright (c) 2022, Chris Ellis
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

## Sponsors

This project is sponsored by [Nexteam](https://nexteam.co.uk/)
