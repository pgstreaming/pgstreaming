package org.pgstreaming;

import java.nio.ByteBuffer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import org.pgstreaming.decoder.DecoderPlugin;
import org.pgstreaming.decoder.DecoderPluginOutputParser;
import org.pgstreaming.metrics.PGStreamMetrics;
import org.pgstreaming.model.EventType;
import org.pgstreaming.model.LSNHolder;
import org.pgstreaming.model.PGLSNStreamPosition;
import org.pgstreaming.model.PGStreamRecord;
import org.pgstreaming.model.StreamPosition;
import org.pgstreaming.model.record.Record;
import org.postgresql.PGConnection;
import org.postgresql.PGProperty;
import org.postgresql.replication.LogSequenceNumber;
import org.postgresql.replication.PGReplicationStream;

import com.codahale.metrics.Histogram;
import com.codahale.metrics.Meter;
import com.codahale.metrics.Timer;

public class PGStreamingConnector<K,V>
{
    // base config
    
    private final String url;
    
    private final String username;
    
    private final String password;
    
    private final String slotName;
    
    // slot plugin and parser
    
    private final DecoderPlugin<?> plugin;
    
    private final DecoderPluginOutputParser<K, V> parser;
    
    // runtime state
    
    private final AtomicBoolean connected = new AtomicBoolean(false);
    
    private Connection connection;
    
    private PGReplicationStream stream;
    
    private AtomicBoolean run = new AtomicBoolean(false);
    
    private volatile CountDownLatch latch;
    
    // metrics
    
    private final Timer parseTimer;
    
    private final Timer commitTimer;
    
    private final Timer consumeTimer;
    
    private final Meter consumeErrors;
    
    private final Meter consumeBytes;
    
    private final Histogram messageSize;
    
    public PGStreamingConnector(String url, String username, String password, String slotName, DecoderPlugin<?> plugin, DecoderPluginOutputParser<K, V> parser)
    {
        super();
        this.url = Objects.requireNonNull(url);
        this.username = Objects.requireNonNull(username);
        this.password = Objects.requireNonNull(password);
        this.slotName = Objects.requireNonNull(slotName);
        this.plugin = Objects.requireNonNull(plugin);
        this.parser = Objects.requireNonNull(parser);
        // metrics
        this.parseTimer = PGStreamMetrics.getRegistry().timer("pgstreaming.connector.parse[slot=" + slotName + "]");
        this.commitTimer = PGStreamMetrics.getRegistry().timer("pgstreaming.connector.commit[slot=" + slotName + "]");
        this.consumeTimer = PGStreamMetrics.getRegistry().timer("pgstreaming.connector.consume_process[slot=" + slotName + "]");
        this.consumeErrors = PGStreamMetrics.getRegistry().meter("pgstreaming.connector.consume_errors[slot=" + slotName + "]");
        this.consumeBytes = PGStreamMetrics.getRegistry().meter("pgstreaming.connector.consume_bytes[slot=" + slotName + "]");
        this.messageSize = PGStreamMetrics.getRegistry().histogram("pgstreaming.connector.message_size[slot=" + slotName + "]");
    }
    
    public static <K, V> PGStreamingConnector<K, V> create(String url, String username, String password, String slotName, DecoderPlugin<?> plugin, Class<K> keyType, Class<V> valueType)
    {
        return new PGStreamingConnector<>(url, username, password, slotName, plugin, plugin.createParser(keyType, valueType));
    }
    
    public static PGStreamingConnector<Record, Record> create(String url, String username, String password, String slotName, DecoderPlugin<?> plugin)
    {
        return create(url, username, password, slotName, plugin, Record.class, Record.class);
    }
    
    public PGStreamingConnector<K,V> connect() throws Exception
    {
        if (connected.compareAndSet(false, true))
        {
            // connect to the server with the replication protocol
            Properties props = new Properties();
            PGProperty.ASSUME_MIN_SERVER_VERSION.set(props, "9.4");
            PGProperty.REPLICATION.set(props, "database");
            PGProperty.PREFER_QUERY_MODE.set(props, "simple");
            PGProperty.USER.set(props, this.username);
            PGProperty.PASSWORD.set(props, this.password);
            this.connection = DriverManager.getConnection(this.url, props);
            // open the replication stream
            this.stream = ((PGConnection) this.connection).getReplicationAPI()
                .replicationStream()
                .logical()
                .withSlotName(this.slotName)
                .withSlotOptions(this.plugin.configureSlot())
                .withStartPosition(LogSequenceNumber.valueOf("0/0"))
                .start();
            //
        }
        return this;
    }
    
    protected ByteBuffer read() throws Exception
    {
        final ByteBuffer event = this.stream.read();
        if (event == null) return null;
        this.consumeBytes.mark(event.remaining());
        this.messageSize.update(event.remaining());
        return event;
    }
    
    protected ByteBuffer readPending() throws Exception
    {
        final ByteBuffer event = this.stream.readPending();
        if (event == null) return null;
        this.consumeBytes.mark(event.remaining());
        this.messageSize.update(event.remaining());
        return event;
    }
    
    protected Collection<PGStreamRecord<K,V>> parse(ByteBuffer event) throws Exception
    {
        if (event == null) return Collections.emptyList();
        // parse the message
        try (Timer.Context tctx = this.parseTimer.time())
        {
            StreamPosition lsn = new PGLSNStreamPosition(this.stream.getLastReceiveLSN());
            return this.parser.parse(event, lsn);
        }   
    }
    
    public Collection<PGStreamRecord<K,V>> poll() throws Exception
    {
        return parse(readPending());
    }
    
    public Collection<PGStreamRecord<K,V>> take() throws Exception
    {
        return parse(read());
    }
    
    public void commit(StreamPosition position) throws Exception
    {
        try (Timer.Context tctx = this.commitTimer.time())
        {
            if (position instanceof LSNHolder)
            {
                LogSequenceNumber lsn = ((LSNHolder) position).lsn();
                    this.stream.setAppliedLSN(lsn);
                    this.stream.setFlushedLSN(lsn);
                    this.stream.forceUpdateStatus();
            }
        }
    }
    
    public void commit(PGStreamRecord<K,V> record) throws Exception
    {
        this.commit(record.position());
    }
    
    public void consume(Consumer<PGStreamRecord<K,V>> consumer) throws Exception
    {
        if (this.run.compareAndSet(false, true))
        {
            this.latch = new CountDownLatch(1);
            try
            {
                while (this.run.get())
                {
                    Collection<PGStreamRecord<K,V>> records = this.poll();
                    // process the records
                    for (PGStreamRecord<K,V> record : records)
                    {
                        // process the record
                        try (Timer.Context tctx = this.consumeTimer.time())
                        {
                            if (record.eventType() != null && record.eventType() != EventType.BEGIN && record.eventType() != EventType.COMMIT)
                            {
                                try
                                {
                                    consumer.accept(record);
                                }
                                catch (Exception e)
                                {
                                    this.consumeErrors.mark();
                                    throw e;
                                }
                            }
                        }
                        // commit the LSN 
                        this.commit(record);
                    }
                    // pause if we've not got any records to process
                    if (records.isEmpty())
                        TimeUnit.MILLISECONDS.sleep(1);
                }
            }
            finally
            {
                this.latch.countDown();
            }
        }
    }
    
    public void consumeParallel(Consumer<PGStreamRecord<K,V>> consumer) throws Exception
    {
        if (this.run.compareAndSet(false, true))
        {
            this.latch = new CountDownLatch(1);
            try
            {
                while (this.run.get())
                {
                    
                    final ByteBuffer event = this.readPending();
                    // skip empty records
                    if (event == null)
                    {
                        TimeUnit.MILLISECONDS.sleep(1);
                        continue;
                    }
                    // dispatch parse and consume task
                    CompletableFuture.runAsync(() -> {
                        try
                        {
                            Collection<PGStreamRecord<K,V>> records = this.parse(event);
                            // process the records
                            for (PGStreamRecord<K,V> record : records)
                            {
                                // process the record
                                try (Timer.Context tctx = this.consumeTimer.time())
                                {
                                    if (record.eventType() != null && record.eventType() != EventType.BEGIN && record.eventType() != EventType.COMMIT)
                                    {
                                        consumer.accept(record);
                                    }
                                }
                                // commit the LSN 
                                this.commit(record); 
                            }
                        }
                        catch (Exception e)
                        {
                            this.consumeErrors.mark();
                        }
                    });
                }
            }
            finally
            {
                this.latch.countDown();
            }
        }
    }
    
    public void await()
    {
        if (this.run.get())
        {
            while (this.latch == null)
            {
                // spin
            }
            // await the latch
            try
            {
                this.latch.await();
            }
            catch (InterruptedException e)
            {
            }
        }
    }
    
    public void close()
    {
        if (this.run.compareAndSet(true, false))
        {
            while (this.latch == null)
            {
                // spin
            }
            // await the latch to signal
            try
            {
                this.latch.await();
            }
            catch (InterruptedException e)
            {
            }
        }
        // shutdown the connection
        try
        {
            if (this.stream != null)
            {
                this.stream.close();
                this.stream = null;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        try
        {
            if (this.connection != null)
            {
                this.connection.close();
                this.connection = null;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
}
