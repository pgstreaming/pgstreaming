package org.pgstreaming.model;

import org.postgresql.replication.LogSequenceNumber;

public interface LSNHolder 
{
    LogSequenceNumber lsn();   
}
