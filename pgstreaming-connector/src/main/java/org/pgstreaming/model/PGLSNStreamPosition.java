package org.pgstreaming.model;

import org.pgstreaming.model.StreamPosition;
import org.postgresql.replication.LogSequenceNumber;

public class PGLSNStreamPosition implements StreamPosition, LSNHolder
{
    private final LogSequenceNumber lsn;
    
    public PGLSNStreamPosition(LogSequenceNumber lsn)
    {
        super();
        this.lsn = lsn;
    }
    
    @Override
    public LogSequenceNumber lsn()
    {
        return lsn;
    }
    
    @Override
    public String toString()
    {
        return "LSN" + this.lsn.toString();
    }
}
