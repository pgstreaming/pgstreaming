package org.pgstreaming.metrics;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.pgstreaming.metrics.reporter.CSVReporter;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.MetricRegistry;

public final class PGStreamMetrics
{
    private static final MetricRegistry metrics = new MetricRegistry();
    
    private static ConsoleReporter consoleReporter;
    
    private static CSVReporter csvReporter;
    
    public static final MetricRegistry getRegistry()
    {
        return metrics;
    }
    
    public static synchronized void enableConsoleReporter()
    {
        if (consoleReporter == null)
        {
            consoleReporter = ConsoleReporter.forRegistry(metrics)
                    .convertRatesTo(TimeUnit.SECONDS)
                    .convertDurationsTo(TimeUnit.MICROSECONDS)
                    .build();
            consoleReporter.start(30, TimeUnit.SECONDS);
        }
    }
    
    public static synchronized void enableCSVReporter(File output)
    {
        if (csvReporter == null)
        {
            csvReporter = CSVReporter.forRegistry(metrics)
                    .convertRatesTo(TimeUnit.SECONDS)
                    .convertDurationsTo(TimeUnit.MICROSECONDS)
                    .outputTo(output)
                    .build();
            csvReporter.start(30, TimeUnit.SECONDS);
        }
    }
    
    public static synchronized void enableCSVReporter()
    {
        if (csvReporter == null)
        {
            csvReporter = CSVReporter.forRegistry(metrics)
                    .convertRatesTo(TimeUnit.SECONDS)
                    .convertDurationsTo(TimeUnit.MICROSECONDS)
                    .build();
            csvReporter.start(30, TimeUnit.SECONDS);
        }
    }
}
