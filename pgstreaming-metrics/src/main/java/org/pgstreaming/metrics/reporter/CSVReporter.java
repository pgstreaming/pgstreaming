package org.pgstreaming.metrics.reporter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TimeZone;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.codahale.metrics.Clock;
import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.Counter;
import com.codahale.metrics.Gauge;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricAttribute;
import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.ScheduledReporter;
import com.codahale.metrics.Snapshot;
import com.codahale.metrics.Timer;

/**
 * A reporter which outputs measurements to a stream in CSV format.
 * 
 * Based on {@link ConsoleReporter}
 */
public class CSVReporter extends ScheduledReporter {
    /**
     * Returns a new {@link Builder} for {@link ConsoleReporter}.
     *
     * @param registry the registry to report
     * @return a {@link Builder} instance for a {@link ConsoleReporter}
     */
    public static Builder forRegistry(MetricRegistry registry) {
        return new Builder(registry);
    }

    /**
     * A builder for {@link ConsoleReporter} instances. Defaults to using the default locale and
     * time zone, writing to {@code System.out}, converting rates to events/second, converting
     * durations to milliseconds, and not filtering metrics.
     */
    public static class Builder {
        private final MetricRegistry registry;
        private Writer output;
        private Locale locale;
        private Clock clock;
        private TimeZone timeZone;
        private TimeUnit rateUnit;
        private TimeUnit durationUnit;
        private MetricFilter filter;
        private ScheduledExecutorService executor;
        private boolean shutdownExecutorOnStop;
        private Set<MetricAttribute> disabledMetricAttributes;

        private Builder(MetricRegistry registry) {
            this.registry = registry;
            this.output = new OutputStreamWriter(System.out);
            this.locale = Locale.getDefault();
            this.clock = Clock.defaultClock();
            this.timeZone = TimeZone.getDefault();
            this.rateUnit = TimeUnit.SECONDS;
            this.durationUnit = TimeUnit.MILLISECONDS;
            this.filter = MetricFilter.ALL;
            this.executor = null;
            this.shutdownExecutorOnStop = true;
            disabledMetricAttributes = Collections.emptySet();
        }

        /**
         * Specifies whether or not, the executor (used for reporting) will be stopped with same time with reporter.
         * Default value is true.
         * Setting this parameter to false, has the sense in combining with providing external managed executor via {@link #scheduleOn(ScheduledExecutorService)}.
         *
         * @param shutdownExecutorOnStop if true, then executor will be stopped in same time with this reporter
         * @return {@code this}
         */
        public Builder shutdownExecutorOnStop(boolean shutdownExecutorOnStop) {
            this.shutdownExecutorOnStop = shutdownExecutorOnStop;
            return this;
        }

        /**
         * Specifies the executor to use while scheduling reporting of metrics.
         * Default value is null.
         * Null value leads to executor will be auto created on start.
         *
         * @param executor the executor to use while scheduling reporting of metrics.
         * @return {@code this}
         */
        public Builder scheduleOn(ScheduledExecutorService executor) {
            this.executor = executor;
            return this;
        }

        /**
         * Write to the given {@link Writer}.
         *
         * @param output a {@link Writer} instance.
         * @return {@code this}
         */
        public Builder outputTo(Writer output) {
            this.output = output;
            return this;
        }
        
        /**
         * Write to the given {@link Writer}.
         *
         * @param output a {@link Writer} instance.
         * @return {@code this}
         * @throws IOException 
         */
        public Builder outputTo(File output) {
            try
            {
                this.output = new BufferedWriter(new FileWriter(output));
            }
            catch (IOException e)
            {
                throw new IllegalArgumentException(e);
            }
            return this;
        }

        /**
         * Format numbers for the given {@link Locale}.
         *
         * @param locale a {@link Locale}
         * @return {@code this}
         */
        public Builder formattedFor(Locale locale) {
            this.locale = locale;
            return this;
        }

        /**
         * Use the given {@link Clock} instance for the time.
         *
         * @param clock a {@link Clock} instance
         * @return {@code this}
         */
        public Builder withClock(Clock clock) {
            this.clock = clock;
            return this;
        }

        /**
         * Use the given {@link TimeZone} for the time.
         *
         * @param timeZone a {@link TimeZone}
         * @return {@code this}
         */
        public Builder formattedFor(TimeZone timeZone) {
            this.timeZone = timeZone;
            return this;
        }

        /**
         * Convert rates to the given time unit.
         *
         * @param rateUnit a unit of time
         * @return {@code this}
         */
        public Builder convertRatesTo(TimeUnit rateUnit) {
            this.rateUnit = rateUnit;
            return this;
        }

        /**
         * Convert durations to the given time unit.
         *
         * @param durationUnit a unit of time
         * @return {@code this}
         */
        public Builder convertDurationsTo(TimeUnit durationUnit) {
            this.durationUnit = durationUnit;
            return this;
        }

        /**
         * Only report metrics which match the given filter.
         *
         * @param filter a {@link MetricFilter}
         * @return {@code this}
         */
        public Builder filter(MetricFilter filter) {
            this.filter = filter;
            return this;
        }

        /**
         * Don't report the passed metric attributes for all metrics (e.g. "p999", "stddev" or "m15").
         * See {@link MetricAttribute}.
         *
         * @param disabledMetricAttributes a {@link MetricFilter}
         * @return {@code this}
         */
        public Builder disabledMetricAttributes(Set<MetricAttribute> disabledMetricAttributes) {
            this.disabledMetricAttributes = disabledMetricAttributes;
            return this;
        }

        /**
         * Builds a {@link ConsoleReporter} with the given properties.
         *
         * @return a {@link ConsoleReporter}
         */
        public CSVReporter build() {
            return new CSVReporter(registry,
                    output,
                    locale,
                    clock,
                    timeZone,
                    rateUnit,
                    durationUnit,
                    filter,
                    executor,
                    shutdownExecutorOnStop,
                    disabledMetricAttributes);
        }
    }

    private final Writer output;
    private final Locale locale;
    private final Clock clock;
    private final DateFormat dateFormat;

    private CSVReporter(MetricRegistry registry,
                            Writer output,
                            Locale locale,
                            Clock clock,
                            TimeZone timeZone,
                            TimeUnit rateUnit,
                            TimeUnit durationUnit,
                            MetricFilter filter,
                            ScheduledExecutorService executor,
                            boolean shutdownExecutorOnStop,
                            Set<MetricAttribute> disabledMetricAttributes) {
        super(registry, "csv-reporter", filter, rateUnit, durationUnit, executor, shutdownExecutorOnStop, disabledMetricAttributes);
        this.output = output;
        this.locale = locale;
        this.clock = clock;
        this.dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT,
                DateFormat.MEDIUM,
                locale);
        dateFormat.setTimeZone(timeZone);
        // print CSV header
        this.printHeader();
    }

    @Override
    @SuppressWarnings("rawtypes")
    public void report(SortedMap<String, Gauge> gauges,
                       SortedMap<String, Counter> counters,
                       SortedMap<String, Histogram> histograms,
                       SortedMap<String, Meter> meters,
                       SortedMap<String, Timer> timers) {
        try
        {
            final String dateTime = dateFormat.format(new Date(clock.getTime())).replace(",", "");
    
            if (!gauges.isEmpty()) {
                for (Map.Entry<String, Gauge> entry : gauges.entrySet()) {
                    output.write(dateTime);
                    output.write(",gauge,");
                    output.write(entry.getKey());
                    printGauge(entry.getValue());
                    output.write("\n");
                }
            }
    
            if (!counters.isEmpty()) {
                for (Map.Entry<String, Counter> entry : counters.entrySet()) {
                    output.write(dateTime);
                    output.write(",counter,");
                    output.write(entry.getKey());
                    printCounter(entry);
                    output.write("\n");
                }
            }
    
            if (!histograms.isEmpty()) {
                for (Map.Entry<String, Histogram> entry : histograms.entrySet()) {
                    output.write(dateTime);
                    output.write(",histogram,");
                    output.write(entry.getKey());
                    printHistogram(entry.getValue());
                    output.write("\n");
                }
            }
    
            if (!meters.isEmpty()) {
                for (Map.Entry<String, Meter> entry : meters.entrySet()) {
                    output.write(dateTime);
                    output.write(",meter,");
                    output.write(entry.getKey());
                    printMeter(entry.getValue());
                    output.write("\n");
                }
            }
    
            if (!timers.isEmpty()) {
                for (Map.Entry<String, Timer> entry : timers.entrySet()) {
                    output.write(dateTime);
                    output.write(",timer,");
                    output.write(entry.getKey());
                    printTimer(entry.getValue());
                    output.write("\n");
                }
            }
            
            output.flush();
        }
        catch (IOException e)
        {
            // oops
        }
    }
    
    private void printHeader()
    {
        try
        {
            this.output.write("Sampled At,Type,Name");
            this.output.write(",Value");
            this.output.write(",Count");
            this.output.write(",Mean Rate (/" + getRateUnit() + ")");
            this.output.write(",1m Rate (/" + getRateUnit() + ")");
            this.output.write(",5m Rate (/" + getRateUnit() + ")");
            this.output.write(",15m Rate (/" + getRateUnit() + ")");
            this.output.write(",Min (" + getDurationUnit() + ")");
            this.output.write(",Max (" + getDurationUnit() + ")");
            this.output.write(",Mean (" + getDurationUnit() + ")");
            this.output.write(",Std Dev (" + getDurationUnit() + ")");
            this.output.write(",P50 (" + getDurationUnit() + ")");
            this.output.write(",P75 (" + getDurationUnit() + ")");
            this.output.write(",P95 (" + getDurationUnit() + ")");
            this.output.write(",P98 (" + getDurationUnit() + ")");
            this.output.write(",P99 (" + getDurationUnit() + ")");
            this.output.write(",P999 (" + getDurationUnit() + ")");
            this.output.write("\n");
        }
        catch (IOException e)
        {
        }
    }

    private void printMeter(Meter meter) throws IOException {
        output.write(",");
        printIfEnabled(MetricAttribute.COUNT, String.format(locale, "%d", meter.getCount()));
        printIfEnabled(MetricAttribute.MEAN_RATE, String.format(locale, "%2.2f", convertRate(meter.getMeanRate())));
        printIfEnabled(MetricAttribute.M1_RATE, String.format(locale, "%2.2f", convertRate(meter.getOneMinuteRate())));
        printIfEnabled(MetricAttribute.M5_RATE, String.format(locale, "%2.2f", convertRate(meter.getFiveMinuteRate())));
        printIfEnabled(MetricAttribute.M15_RATE, String.format(locale, "%2.2f", convertRate(meter.getFifteenMinuteRate())));
        output.write(",,,,,,,,,,");
    }

    private void printCounter(Map.Entry<String, Counter> entry) throws IOException {
        output.write(",");
        output.write(String.format(locale, "%d", entry.getValue().getCount()));
        this.output.write(",,,,,,,,,,,,,,");
    }

    private void printGauge(Gauge<?> gauge) throws IOException {
        output.write(String.format(locale, "%s", gauge.getValue()));
        output.write(",,,,,,,,,,,,,,,");
    }

    private void printHistogram(Histogram histogram) throws IOException {
        final Snapshot snapshot = histogram.getSnapshot();
        output.write(",");
        printIfEnabled(MetricAttribute.COUNT, String.format(locale, "%d", histogram.getCount()));
        this.output.write(",,,,");
        printIfEnabled(MetricAttribute.MIN, String.format(locale, "%d", snapshot.getMin()));
        printIfEnabled(MetricAttribute.MAX, String.format(locale, "%d", snapshot.getMax()));
        printIfEnabled(MetricAttribute.MEAN, String.format(locale, "%2.2f", snapshot.getMean()));
        printIfEnabled(MetricAttribute.STDDEV, String.format(locale, "%2.2f", snapshot.getStdDev()));
        printIfEnabled(MetricAttribute.P50, String.format(locale, "%2.2f", snapshot.getMedian()));
        printIfEnabled(MetricAttribute.P75, String.format(locale, "%2.2f", snapshot.get75thPercentile()));
        printIfEnabled(MetricAttribute.P95, String.format(locale, "%2.2f", snapshot.get95thPercentile()));
        printIfEnabled(MetricAttribute.P98, String.format(locale, "%2.2f", snapshot.get98thPercentile()));
        printIfEnabled(MetricAttribute.P99, String.format(locale, "%2.2f", snapshot.get99thPercentile()));
        printIfEnabled(MetricAttribute.P999, String.format(locale, "%2.2f", snapshot.get999thPercentile()));
    }

    private void printTimer(Timer timer) throws IOException {
        final Snapshot snapshot = timer.getSnapshot();
        output.write(",");
        printIfEnabled(MetricAttribute.COUNT, String.format(locale, "%d", timer.getCount()));
        printIfEnabled(MetricAttribute.MEAN_RATE, String.format(locale, "%2.2f", convertRate(timer.getMeanRate())));
        printIfEnabled(MetricAttribute.M1_RATE, String.format(locale, "%2.2f", convertRate(timer.getOneMinuteRate())));
        printIfEnabled(MetricAttribute.M5_RATE, String.format(locale, "%2.2f", convertRate(timer.getFiveMinuteRate())));
        printIfEnabled(MetricAttribute.M15_RATE, String.format(locale, "%2.2f", convertRate(timer.getFifteenMinuteRate())));
        printIfEnabled(MetricAttribute.MIN, String.format(locale, "%2.2f", convertDuration(snapshot.getMin())));
        printIfEnabled(MetricAttribute.MAX, String.format(locale, "%2.2f", convertDuration(snapshot.getMax())));
        printIfEnabled(MetricAttribute.MEAN, String.format(locale, "%2.2f", convertDuration(snapshot.getMean())));
        printIfEnabled(MetricAttribute.STDDEV, String.format(locale, "%2.2f", convertDuration(snapshot.getStdDev())));
        printIfEnabled(MetricAttribute.P50, String.format(locale, "%2.2f", convertDuration(snapshot.getMedian())));
        printIfEnabled(MetricAttribute.P75, String.format(locale, "%2.2f", convertDuration(snapshot.get75thPercentile())));
        printIfEnabled(MetricAttribute.P95, String.format(locale, "%2.2f", convertDuration(snapshot.get95thPercentile())));
        printIfEnabled(MetricAttribute.P98, String.format(locale, "%2.2f", convertDuration(snapshot.get98thPercentile())));
        printIfEnabled(MetricAttribute.P99, String.format(locale, "%2.2f", convertDuration(snapshot.get99thPercentile())));
        printIfEnabled(MetricAttribute.P999, String.format(locale, "%2.2f", convertDuration(snapshot.get999thPercentile())));
    }

    /**
     * Print only if the attribute is enabled
     *
     * @param type   Metric attribute
     * @param status Status to be logged
     * @throws IOException 
     */
    private void printIfEnabled(MetricAttribute type, String status) throws IOException {
        output.write(",");
        if (getDisabledMetricAttributes().contains(type)) {
            return;
        }
        output.write(status);
    }
}

