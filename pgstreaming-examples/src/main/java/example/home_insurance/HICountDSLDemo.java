package example.home_insurance;

import static org.pgstreaming.PGStreaming.*;

import example.home_insurance.service.PurchaseCounter;

public class HICountDSLDemo
{
    public static void main(String[] args) throws Exception
    {
        // enable metrics logging
        // PGStreamMetrics.enableCSVReporter();
        // counter service
        PurchaseCounter counter = new PurchaseCounter();
        // consume the stream
        pgStreaming()
        .source("policies")
        .ofRecord()
        .from("jdbc:postgresql://127.0.0.1:5414/streaming", "rep", "")
        .slot("purchase_count")
        .wal2json()
        .includeTable("event.policy")
        .then()
        .consume((key, value) -> {
            counter.markSale();
        })
        .register()
        .launch();
    }
    
    
}
