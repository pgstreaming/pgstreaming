CREATE SCHEMA home_insurance;
CREATE SCHEMA event;

CREATE TABLE IF NOT EXISTS home_insurance.address (
  provider    TEXT NOT NULL,
  id          TEXT NOT NULL,
  lines       TEXT[] NOT NULL,
  postcode    TEXT NOT NULL,
  uprn        BIGINT,
  detail      JSONB,
  CONSTRAINT address_pk PRIMARY KEY (provider, id)
);

CREATE TABLE IF NOT EXISTS home_insurance.terms (
  version           INTEGER NOT NULL,
  content           TEXT NOT NULL,
  created           TIMESTAMPTZ NOT NULL,
  updated           TIMESTAMPTZ,
  CONSTRAINT terms_pk PRIMARY KEY (version)
);

CREATE TABLE IF NOT EXISTS home_insurance.customer (
  email            TEXT NOT NULL,
  title            TEXT NOT NULL,
  first_name       TEXT NOT NULL,
  last_name        TEXT NOT NULL,
  date_of_birth    DATE NOT NULL,
  address_provider TEXT NOT NULL,
  address_id       TEXT NOT NULL,
  userid           TEXT,
  terms_version    INTEGER NOT NULL,
  created          TIMESTAMPTZ NOT NULL,
  updated          TIMESTAMPTZ,
  CONSTRAINT customer_pk PRIMARY KEY (email),
  CONSTRAINT address_fk FOREIGN KEY (address_provider, address_id) REFERENCES home_insurance.address (provider, id) ON DELETE RESTRICT ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT terms_fk FOREIGN KEY (terms_version) REFERENCES home_insurance.terms (version) ON DELETE RESTRICT ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TYPE home_insurance.payment_type AS ENUM (
    'DIRECT_DEBIT_ANNUAL',
    'DIRECT_DEBIT_MONTHLY',
    'CARD_ONE_OFF',
    'CARD_ANNUAL',
    'CARD_MONTHLY',
    'ANNUAL',
    'INSTALMENTS',
    'UNKNOWN'
);

CREATE TYPE home_insurance.policy_status AS ENUM (
    'ACTIVE',
    'CANCELLED',
    'ENDED'
);

CREATE TABLE IF NOT EXISTS home_insurance.policy (
  provider                  TEXT NOT NULL,
  reference                 TEXT NOT NULL,
  email                     TEXT NOT NULL,
  address_provider          TEXT NOT NULL,
  address_id                TEXT NOT NULL,
  transaction_date          DATE,
  policy_start_date         DATE NOT NULL,
  policy_renew_date         DATE,
  policy_cancel_date        DATE,
  status                    home_insurance.policy_status NOT NULL,
  payment_method            home_insurance.payment_type NOT NULL,
  price                     NUMERIC NOT NULL,
  contents                  BOOLEAN NOT NULL,
  buildings                 BOOLEAN NOT NULL,
  detail                    JSONB,
  quote_reference           TEXT,
  provider_quote_reference  TEXT,
  commission_received       BOOLEAN DEFAULT FALSE,
  created                   TIMESTAMPTZ NOT NULL,
  updated                   TIMESTAMPTZ,
  CONSTRAINT policy_pk PRIMARY KEY (provider, reference),
  CONSTRAINT customer_fk FOREIGN KEY (email) REFERENCES home_insurance.customer (email) ON DELETE RESTRICT ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT address_fk FOREIGN KEY (address_provider, address_id) REFERENCES home_insurance.address (provider, id) ON DELETE RESTRICT ON UPDATE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE IF NOT EXISTS event.policy (
  id               UUID NOT NULL,
  emitted_at       TIMESTAMPTZ NOT NULL,
  policy_provider  TEXT NOT NULL,
  policy_reference TEXT NOT NULL,
  customer_name    TEXT NOT NULL,
  customer_email   TEXT NOT NULL,
  post_code        TEXT NOT NULL,
  detail           JSON NOT NULL,
  CONSTRAINT policy_event_pk PRIMARY KEY (id)
);

CREATE INDEX policy_event_emitted_at_idx ON event.policy USING brin(emitted_at);


CREATE OR REPLACE FUNCTION policy_purchase()
RETURNS INTEGER LANGUAGE plpgsql AS 
$$
BEGIN
  INSERT INTO event.policy VALUES (gen_random_uuid(), now(), 'A', gen_random_uuid()::TEXT, 'Tess Ting', 'tess@ting.test', 'WV1 2BC', '{}');
  RETURN 1;
END;
$$;
-- INSERT INTO event.policy VALUES (gen_random_uuid(), now(), 'A', gen_random_uuid()::TEXT, 'Tess Ting', 'tess@ting.test', 'WV1 2BC', '{}');


SELECT pg_create_logical_replication_slot('welcome_email', 'wal2json');
SELECT pg_create_logical_replication_slot('purchase_count', 'wal2json');


