package example.home_insurance.service;

import java.util.List;

public class EmailService
{
    
    public EmailService()
    {
        super();
    }

    public void sendWelcomeEmail(String emailAddress, String name, String policyReference, String postcode)
    {
        System.out.println("Sending email to: " + emailAddress + ", with: " + List.of(name, policyReference, postcode));
    }
    
}
