package example.home_insurance.service;

import java.util.concurrent.atomic.AtomicLong;

public class PurchaseCounter
{
    private AtomicLong sales = new AtomicLong();
    
    private long last = System.currentTimeMillis();
    
    public PurchaseCounter()
    {
        super();
    }
    
    public void markSale()
    {
        long count = this.sales.incrementAndGet();
        // log?
        if ((System.currentTimeMillis() - this.last) > 5_000)
        {
            System.out.println("We've sold: " + count + " policies in the last 5 seconds!");
            this.last = System.currentTimeMillis();
        }
    }
}
