package example.test.raw;

import org.pgstreaming.PGStreamingConnector;
import org.pgstreaming.decoder.wal2json.Wal2Json;
import org.pgstreaming.metrics.PGStreamMetrics;
import org.pgstreaming.model.PGStreamRecord;

import example.test.model.TestRecord;
import example.test.model.TestRecordKey;

public class TestTypedParaDemo
{
    public static void main(String[] args) throws Exception
    {
        PGStreamMetrics.enableConsoleReporter();
        // create our connector
        PGStreamingConnector<TestRecordKey, TestRecord> stream = PGStreamingConnector.create("jdbc:postgresql://127.0.0.1:5414/streaming", "rep", "", "rep_test", new Wal2Json(), TestRecordKey.class, TestRecord.class);
        // connect and start streaming from
        stream.connect();
        // read the stream
        Runtime.getRuntime().addShutdownHook(new Thread(() -> stream.close()));
        stream.consumeParallel(TestTypedParaDemo::consume);
    }
    
    public static void consume(PGStreamRecord<TestRecordKey, TestRecord> record)
    {
        // System.out.println(record);
    }
}
