package example.test.raw;

import org.pgstreaming.PGStreamingConnector;
import org.pgstreaming.decoder.wal2json.Wal2Json;
import org.pgstreaming.metrics.PGStreamMetrics;
import org.pgstreaming.model.PGStreamRecord;
import org.pgstreaming.model.record.Record;

public class TestRecordDemo
{
    public static void main(String[] args) throws Exception
    {
        PGStreamMetrics.enableCSVReporter();
        // create our connector
        PGStreamingConnector<Record, Record> stream = PGStreamingConnector.create("jdbc:postgresql://127.0.0.1:5414/streaming", "rep", "", "rep_test", new Wal2Json());
        // connect and start streaming from
        stream.connect();
        // read the stream
        Runtime.getRuntime().addShutdownHook(new Thread(() -> stream.close()));
        stream.consume(TestRecordDemo::consume);
    }
    
    public static void consume(PGStreamRecord<Record, Record> record)
    {
        // System.out.println(record);
    }
}
