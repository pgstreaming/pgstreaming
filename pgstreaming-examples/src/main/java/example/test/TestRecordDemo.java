package example.test;

import static org.pgstreaming.PGStreaming.*;

import org.pgstreaming.metrics.PGStreamMetrics;

public class TestRecordDemo
{
    public static void main(String[] args) throws Exception
    {
        // enable metrics logging
        PGStreamMetrics.enableCSVReporter();
        // consume the stream
        pgStreaming()
        .source("test")
        .ofRecord()
        .from("jdbc:postgresql://127.0.0.1:5414/streaming", "rep", "")
        .slot("rep_test")
        .wal2json()
        .then()
        .consume((key, value) -> {
            System.out.println(key + "=" + value);
        })
        .register()
        .launch();
    }
}
