package example.test.model;

import org.pgstreaming.mapper.annotation.Column;
import org.pgstreaming.mapper.annotation.Primary;

public class TestRecordKey
{
    @Column("a") @Primary
    private int a;
    
    public TestRecordKey()
    {
        super();
    }

    public int getA()
    {
        return this.a;
    }

    public void setA(int a)
    {
        this.a = a;
    }
    public String toString()
    {
        return "TestRecordKey [" + this.getA() + "]";
    }
}
