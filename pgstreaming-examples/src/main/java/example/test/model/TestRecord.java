package example.test.model;

import org.pgstreaming.mapper.annotation.Column;

public class TestRecord extends TestRecordKey
{   
    @Column("b")
    private String b;
    
    @Column("c")
    private String c;
    
    public TestRecord()
    {
        super();
    }

    public String getB()
    {
        return this.b;
    }

    public void setB(String b)
    {
        this.b = b;
    }

    public String getC()
    {
        return this.c;
    }

    public void setC(String c)
    {
        this.c = c;
    }
    
    public String toString()
    {
        return "TestRecord [" + this.getA() + ", " + this.getB() + ", " + this.getC() + "]";
    }
}
