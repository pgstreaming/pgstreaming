package org.pgstreaming.model.impl;

import org.pgstreaming.model.EventType;
import org.pgstreaming.model.PGStreamRecordMeta;
import org.pgstreaming.model.StreamPosition;

public class SimplePGStreamRecordMeta implements PGStreamRecordMeta
{
    private final StreamPosition position;
    
    private final String table;
    
    private final EventType eventType;
    
    public SimplePGStreamRecordMeta(StreamPosition position, String table, EventType eventType)
    {
        super();
        this.position = position;
        this.table = table;
        this.eventType = eventType;
    }
    
    public final StreamPosition position()
    {
        return this.position;
    }
    
    public final String table()
    {
        return this.table;
    }
    
    public final EventType eventType()
    {
        return this.eventType;
    }
    
    public String toString()
    {
        return this.position + " " + this.eventType + " " + this.table;
    }
}
