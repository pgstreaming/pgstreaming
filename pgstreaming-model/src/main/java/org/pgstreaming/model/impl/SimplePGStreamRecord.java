package org.pgstreaming.model.impl;

import org.pgstreaming.model.EventType;
import org.pgstreaming.model.PGStreamRecord;
import org.pgstreaming.model.StreamPosition;

public class SimplePGStreamRecord<K, V> extends SimplePGStreamRecordMeta implements PGStreamRecord<K, V>
{   
    private final K key;
    
    private final V value;
    
    public SimplePGStreamRecord(StreamPosition position, String table, EventType eventType, K key, V value)
    {
        super(position, table, eventType);
        this.key = key;
        this.value = value;
    }

    public K key()
    {
        return this.key;
    }
    
    public V value()
    {
        return this.value;
    }
    
    public String toString()
    {
        return super.toString() + ": " + this.key + "=" + this.value;
    }
}
