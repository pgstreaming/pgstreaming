package org.pgstreaming.model;

/**
 * Metadata for a record in PGStreaming
 */
public interface PGStreamRecordMeta
{
    /**
     * The position of this record in the stream
     */
    StreamPosition position();
    
    /**
     * The table name, from which this event originates
     */
    String table();
    
    /**
     * The type of this event
     */
    EventType eventType();
}
