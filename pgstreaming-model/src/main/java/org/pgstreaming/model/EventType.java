package org.pgstreaming.model;

public enum EventType {
    
    /**
     * The start of a changeset
     */
    BEGIN, 
    
    /**
     * A record being insert
     */
    INSERT, 
    
    /**
     * A record being update
     */
    UPDATE, 
    
    /**
     * A record being deleted
     */
    DELETE,
    
    /**
     * The end of a changeset
     */
    COMMIT
    
}