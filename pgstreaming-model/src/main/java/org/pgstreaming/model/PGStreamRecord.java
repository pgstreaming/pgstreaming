package org.pgstreaming.model;

/**
 * Base record type for PGStreaming
 *
 * @param <K> the key type
 * @param <V> the value type
 */
public interface PGStreamRecord<K, V> extends PGStreamRecordMeta
{
    K key();
    
    V value();   
}
