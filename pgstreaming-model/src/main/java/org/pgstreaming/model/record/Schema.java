package org.pgstreaming.model.record;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Supplier;

/**
 * Schema descriptor for a generic record
 */
public interface Schema
{    
    String name();
    
    Class<?> type();
    
    default boolean isValid(Object value)
    {
        return value == null ? true : type().isInstance(value);
    }
    
    String PgType();
    
    Schema[] fields();
    
    default int length()
    {
        return fields().length;
    }
    
    int fieldIndex(String name);
    
    default Schema field(String name)
    {
        int idx = fieldIndex(name);
        return idx < 0 ? null : fields()[idx];
    }
    
    default Schema field(int idx)
    {
        return idx < 0 ? null : fields()[idx];
    }
    
    public static final Schema[] NO_FIELDS = new Schema[0];
    
    public static Schema of(final String name, final Class<?> type, final String pgType)
    {
        return new Schema()
        {
            @Override
            public String name()
            {
                return name;
            }

            @Override
            public Class<?> type()
            {
                return type;
            }

            @Override
            public String PgType()
            {
                return pgType;
            }

            @Override
            public Schema[] fields()
            {
                return NO_FIELDS;
            }

            @Override
            public int fieldIndex(String name)
            {
                return -1;
            }
            
            public String toString()
            {
                return name + " " + type.getSimpleName();
            }
        };
    }
    
    public static Schema of(final String name, final String pgType, Schema... fields)
    {
        final Map<String, Integer> fieldNames = new HashMap<>();
        for (int i = 0; i < fields.length; i++)
        {
            fieldNames.put(fields[i].name(), Integer.valueOf(i));
        }
        return new Schema()
        {
            @Override
            public String name()
            {
                return name;
            }

            @Override
            public Class<?> type()
            {
                return Record.class;
            }

            @Override
            public String PgType()
            {
                return pgType;
            }

            @Override
            public Schema[] fields()
            {
                return fields;
            }

            @Override
            public int fieldIndex(String name)
            {
                Integer index = fieldNames.get(name);
                return index == null ? -1 : index;
            }
            
            public String toString()
            {
                return name + " Record " + Arrays.toString(fields);
            }
        };
    }
    
    /**
     * Cache of Schemas
     */
    public static class Cached
    {
        private static final ConcurrentMap<String, Schema> cache = new ConcurrentHashMap<>();
        
        public static final Schema of(String name, Supplier<Schema> schemaSupplier)
        {
            return cache.computeIfAbsent(name, (key) -> schemaSupplier.get());
        }
    }
}
