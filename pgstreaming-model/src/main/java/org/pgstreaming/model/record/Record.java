package org.pgstreaming.model.record;

import java.util.Arrays;

/**
 * A generic data record, with a schema and data
 */
public class Record
{
    private final Schema schema;
    
    private final Object[] fields;
    
    public Record(Schema schema)
    {
        super();
        if (schema == null) throw new IllegalArgumentException("Invalid record schema");
        this.schema = schema;
        this.fields = new Object[this.schema.length()];
    }
    
    /**
     * Get the schema for this record
     * @return the {@link Schema} of this record
     */
    public final Schema schema()
    {
        return schema;
    }
    
    /**
     * Get the number of fields in this record
     */
    public final int length()
    {
        return fields.length;
    }
    
    /**
     * Get a field value of this record by index
     * @param <T> the resultant type to which the value will be cast (unchecked)
     * @param index the field index
     * @return the field value
     */
    @SuppressWarnings("unchecked")
    public final <T> T get(int index)
    {
        return index < 0 ? null : (T) this.fields[index];
    }
    
    /**
     * Get a field value of this record by name
     * @param <T> the resultant type to which the value will be cast (unchecked)
     * @param name the field name
     * @return the field value
     */
    public final <T> T get(String name)
    {
        return get(this.schema.fieldIndex(name));
    }
    
    /**
     * Set the value of a field of this record by index
     * @param index the field index
     * @param value the new field value
     */
    public final void set(int index, Object value)
    {
        Schema fieldSchema = this.schema.field(index);
        if (fieldSchema != null && fieldSchema.isValid(value))
            this.fields[index] = value;
    }
    
    /**
     * Set the value of a field of this record by name
     * @param name the field name
     * @param value the new field value
     */
    public final void set(String name, Object value)
    {
        this.set(this.schema.fieldIndex(name), value);
    }
    
    /**
     * Describe this record
     */
    public String toString()
    {
        return this.schema + " = " + Arrays.toString(this.fields);
    }
}
